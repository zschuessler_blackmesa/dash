<?php

return [
    'name' => 'Default Dash Theme',
    'slug' => 'default',
    'body-class-rules' => [
        \Dash\Modules\Theme\Rules\BodyClasses\RoutePathGenerator::class
    ]
];

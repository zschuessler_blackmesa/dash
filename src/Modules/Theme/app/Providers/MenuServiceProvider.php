<?php

namespace Dash\Modules\Theme\Providers;

use Dash\Module\Module;
use Dash\Modules\Theme\Providers\MenuServiceProvider\MenuItem;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * @var $items Collection
     */
    public $items;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->items = collect();
        $this->app->instance(MenuServiceProvider::class, $this);

        // After app is bootstrapped, compile the default menu
        $this->app->booted(function() {
            $this->registerDefaultMenu();
        });
    }

    public function items()
    {
        return $this->items;
    }

    public function getMenuItems()
    {
        return collect($this->items);
    }

    public function addMenuItem(MenuItem $menuItem)
    {
        $this->items->push($menuItem);

        return $this;
    }

    public function registerDefaultMenu()
    {
        $defaultTitle = ucwords(config('dash.default_route_namespace'));

        // Get any existing menu items to add
        $menuItems = collect($this->items);

        // Register top-most menu items first
        $topMostMenuItems = collect();
        $topMostMenuItems->push(new MenuItem([
            'title' => $defaultTitle,
            'route' => config('dash.default_route_namespace'),
            'icon' => 'fa fa-home'
        ]));

        // Media Library
        $mediaConfig = collect(config('dash-media-library'));
        $topMostMenuItems->push(new MenuItem($mediaConfig->get('dash-menu')));

        // User-defined modules
        $appConfig = collect(config()->all());
        $appModules = collect(\Module::all())
            ->filter(function(Module $module) use($appConfig) {
                // Dont show disabled modules
                if ($module->disabled()) {
                    return;
                }

                // Don't show modules that are not set to visible
                $configKey = config(sprintf('%s-module.dash-menu.visible', $module->getSlug()));
                if ($configKey) {
                    return true;
                }
            })
            ->transform(function(Module $module) use ($appConfig) {
                $routeBasepath = sprintf(
                    '%s.%s',
                    config('dash.default_route_namespace'),
                    Str::slug(Str::kebab($module->getName()))
                );
                $configKey = sprintf('%s-module', strtolower($module->getSlug()));
                $defaultModuleConfig = [
                    'title'    => $module->getName(),
                    'visible'  => true,
                    'icon'     => 'fas fa-box',
                    'priority' => 100,
                    'route'    => sprintf('%s.index', $routeBasepath)
                ];

                $moduleConfig = array_merge(
                    $defaultModuleConfig,
                    data_get($appConfig->get($configKey), 'dash-menu')
                );

                /**
                 * Add Children
                 */
                $parentItem = new MenuItem($moduleConfig);

                return $parentItem;
            });

        $menuItems = $menuItems->merge($appModules);

        // Merge
        $this->items = $topMostMenuItems->merge($menuItems);

        return $this;
    }

    public function provides()
    {
        return [MenuServiceProvider::class];
    }
}

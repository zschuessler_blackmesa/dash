<?php

namespace Dash\Modules\Theme\Providers;

use Illuminate\Support\ServiceProvider;
use Dash\Modules\Theme\Theme;
use Illuminate\Support\Facades\Blade;

class ThemeServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public $resourcesBasePath;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->resourcesBasePath = __DIR__ . '/../../resources/';
        $this->registerConfig();
        $this->registerViews();
        $this->registerPublicAssets();
        $this->registerFacade();
        $this->registerDependents();
    }

    public function registerFacade()
    {
        $this->app->singleton('theme', function ($app) {
            return new Theme;
        });

        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('Theme', \Dash\Modules\Theme\Facade::class);
    }

    public function registerDependents()
    {
        $this->app->register(AssetServiceProvider::class);
        $this->app->register(MenuServiceProvider::class);
    }

    public function getResourcePath($resourcePath)
    {
        return __DIR__ . '/../../resources/' . $resourcePath;
    }

    public function registerPublicAssets()
    {
        $this->publishes([
            __DIR__ . '/../../public/' => public_path('dash/theme/default'),
        ], 'public');
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__ . '/../../config/dash-theme.php' => config_path('dash-theme.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/dash-theme.php', 'dash-theme'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $this->publishes(
            [$this->getResourcePath('views') => resource_path('views/dash/theme')],
            'views'
        );

        $viewPaths = [
            $this->getResourcePath('views'),
            resource_path('views/dash')
        ];

        $this->loadViewsFrom($viewPaths, 'theme');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [ThemeServiceProvider::class];
    }
}

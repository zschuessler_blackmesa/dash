<?php

namespace Dash\Modules\Theme\Providers\MenuServiceProvider;

use Illuminate\Http\Request;

class MenuItem
{
    public $title;

    public $attributes;

    public $icon = 'fa fa-dashboard';

    public $classes;

    public $children;

    public $route;

    public function __construct($options = null)
    {
        $this->title = data_get($options, 'title');
        $this->attributes = data_get($options, 'attributes') ?? collect();
        $this->classes = data_get($options, 'classes') ?? collect(['menu-item']);
        $this->route = data_get($options, 'route') ?? '';
        $this->icon = data_get($options, 'icon') ?? $this->icon;
        $this->children = data_get($options, 'children') ?? collect();
    }

    public function attributes()
    {

    }

    public function route()
    {
        if ($this->route) {
            return route($this->route);
        }
    }

    public function title()
    {
        return $this->title;
    }

    public function classes()
    {
        return $this->classes->implode(' ');
    }

    public function icon()
    {
        return $this->icon;
    }

    public function addChild(MenuItem $menuItem)
    {
        $this->children->push($menuItem);

        return $this;
    }
    
    public function matchesActiveRoute()
    {
        $routeName = request()->route()->getName();

        // Check if route directly matches this item
        if ($routeName === $this->route) {
            return true;
        }

        // Check if route matches a child item
        $childItemIsActive = (bool)$this->children->filter(function(MenuItem $menuChildItem) use ($routeName) {
                if ($routeName === $menuChildItem->route) {
                    return true;
                }
            })
            ->count();

        if ($childItemIsActive) {
            return true;
        }

        // Check if its a parent item, and base prefix matches parent item (eg dash.article.edit matches dash.article.
        if ($this->children->count()) {
            $baseParam = collect(request()->route()->parameters())->keys()->first();

            if (!$baseParam) {
                return false;
            }
            $basePath = sprintf('%s.%s.index',  config('dash.default_route_namespace'), str_plural($baseParam));

            if ($basePath === $this->route) {
                return true;
            }
        }
    }
}
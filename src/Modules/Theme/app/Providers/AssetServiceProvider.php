<?php

namespace Dash\Modules\Theme\Providers;

use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;

class AssetServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * @var $items Collection
     */
    public $styles;

    public $scripts;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {

    }

    public function registerScript($path)
    {
        $this->scripts->push($path);

        return $this;
    }

    public function registerStyle($path)
    {
        $this->styles->push($path);

        return $this;
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->instance(AssetServiceProvider::class, $this);
        $this->styles = collect();
        $this->scripts = collect();
    }

    public function styles()
    {
        return $this->styles;
    }

    public function scripts()
    {
        return $this->scripts;
    }

    public function provides()
    {
        return [AssetServiceProvider::class];
    }
}

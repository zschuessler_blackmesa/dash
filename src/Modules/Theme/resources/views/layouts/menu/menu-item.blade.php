<li class="{{ $menuItem->classes }}">
    <a @foreach($menuItem->attributes as $attKey => $attrValue) {{ $attrKey }}="{{ $attrValue }}" @endforeach >
        @if($menuItem->icon)
            <span class="fa fa-dashboard"></span>
        @endif

        {{ $menuItem->title }}
    </a>
</li>
<?php
/**
 * Layout: Sidebar
 *
 * @see \Dash\Modules\Theme\Providers\MenuServiceProvider
 */
?>

<nav class="page-sidebar">
    <div class="page-sidebar-items">
        <div class="page-sidebar-item is-menu-title">
            <a href="/">
                <span class="fas fa-chart-network"></span>
                Dash
            </a>
        </div>
        @foreach(Theme::menu()->items() as $menuItem)
            <?php /** @var $menuItem \Dash\Modules\Theme\Providers\MenuServiceProvider\MenuItem */ ?>
            <div class="page-sidebar-item{{ $menuItem->matchesActiveRoute() ? ' is-active' : '' }}">
                <a href="{{ $menuItem->route() ?? '#' }}" class="{{ $menuItem->classes() }}">
                    @if ($menuItem->icon)
                        <span class="{{ $menuItem->icon() }}"></span>
                    @endif
                    <span class="text">{{ $menuItem->title() }}</span>
                </a>

                @if ($menuItem->children->count())
                    <div class="page-sidebar-submenu">
                        @foreach($menuItem->children as $childItem)
                            <a href="{{ $childItem->route() ?? '#' }}"
                               class="{{ $childItem->matchesActiveRoute() ? 'is-active ' : '' }}{{ $childItem->classes() }}">
                                @if ($childItem->icon)
                                    <span class="{{ $childItem->icon() }}"></span>
                                @endif
                                <span class="text">{{ $childItem->title() }}</span>
                            </a>
                        @endforeach
                    </div>
                @endif
            </div>
        @endforeach
    </div>
</nav>

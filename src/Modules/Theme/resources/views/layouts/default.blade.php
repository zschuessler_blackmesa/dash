<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- Theme Styles load first --}}
    <link rel="stylesheet" href="{{ asset('dash/theme/default/vendor/font-awesome-4.7.0/css/font-awesome.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('dash/theme/default/theme.css') }}"/>

    @foreach(Theme::assets()->styles() as $style)
        <link rel="stylesheet" href="{{ $style }}">
    @endforeach
    <title>Dash Default Theme</title>

    @stack('before_head_end')
</head>
<body class="{{ Theme::bodyClasses()->generateClassString() }}">
<main id="vue-wrapper" class="page-layout">

    {{-- Sidebar --}}
    @include('theme::layouts.default.sidebar')

    {{-- Page --}}
    <div class="page-content">
        @include('theme::layouts.default.top-navigation')

        @yield('content')
    </div>
</main>

<script src="{{ asset('dash/theme/default/vendor.js') }}"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var Vue = window.Vue;
</script>

<script src="{{ asset('dash/theme/default/theme.js') }}"></script>

@foreach(Theme::assets()->scripts() as $script)
    <script src="{{ $script }}"></script>
@endforeach


@if (\Alert::messages() && isset(\Alert::messages()['toast']))
    <script>
        Swal.fire({
            toast: true,
            position: 'bottom-right',
            showConfirmButton: false,
            title: 'Success',
            type: 'success',
            backdrop: true,
            width: '100%',
            text: '{{ \Alert::messages()['toast'][0] ?? 'Success' }}',
            padding: '30px',
            timer: 3200,
        });
    </script>
@endif

@stack('before_body_end')

{{-- Load Vue --}}
<script>
    $(document).ready(function() {

    })
</script>

</body>
</html>
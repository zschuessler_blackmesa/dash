@extends('theme::layouts.default')

@section('content')
    <div class="row">
        <div class="col-xs-12 m-t-25 m-b-25">
            <h1 class="p-b-15">Hello World</h1>

            <p>
                This view is loaded from module: {!! config('theme.name') !!}
                <span class="fa fa-chevron-right"></span>
            </p>
        </div>
    </div>
@endsection
<?php
/**
 * Panel
 *
 * @var $title string
 * @var $subTitle string
 * @var $content string
 *
 * @see \Modules\Theme\Views\Panel
 */
?>
<div class="component-box p-15">
    @if ($title)
        <div class="title">
            <h5>
                {{ $title }}

                @if ($subTitle)
                    <small class="m-l-sm">
                        {{ $subTitle }}
                    </small>
                @endif
            </h5>
        </div>
    @endif

    <div class="content">
        @if(isset($content) && $content instanceof \Illuminate\Support\Collection)
            @foreach($content as $content)
                @include($content->view, $content->data)
            @endforeach
        @else
            @include($content->view, $content->data)
        @endif
    </div>
</div>

<?php
/**
 * Datatable
 *
 * @var $title string
 * @var $columns \Illuminate\Support\Collection
 * @var $rows \Illuminate\Support\Collection
 *
 * @see \Dash\Modules\Crud\ViewModel\Datatable
 */
?>

<div class="component-datatable">
    @if($ajaxSearchRoute)
        <div class="col-xs-12 col-sm-4 p-15 is-flex">
            <input type="text" class="search-input form-control" placeholder="{{ $searchText ?? 'Search...'  }}">
        </div>
    @endif
    <table class="table-datatables component-datatable col-12 m-15">
        <thead>
        <tr>
            @foreach ($columns as $colId => $colLabel)
                <th>{{ $colLabel }}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach ($rows as $row)
            <tr>
                @foreach ($row as $field)
                    <?php /** @var $field \Dash\Modules\Crud\Field */ ?>
                    <td>
                        {!! $field->renderView('datatable') !!}
                    </td>
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
</div>


@push('before_body_end')
    <script>
        var $table = $('.table-datatables').DataTable({
            lengthChange: false,
            dom: '<"p-0"><t><p>',
            processing: true,
            search: true,
            editRoutes: [],

            @if($ajaxSearchRoute)
            serverSide: true,
            ajax: {
                url: '{!! $ajaxSearchRoute !!}',
                type: 'POST',
                data: function (jsonData) {
                    return $.extend({}, jsonData, {
                        _token: '{{ csrf_token() }}',
                        resource_model: "{!! urlencode(\get_class($resourceModel)) !!}",
                        model_class: "{!! urlencode(\get_class($resourceModel->model)) !!}",
                        handler: "{{ urlencode(\Dash\Modules\Crud\Http\Ajax\CrudSearchHandler::class) }}"
                    });
                },
                complete: function(response) {
                    $table.editRoutes = response.responseJSON.editRoutes;
                    var $container = $('.table-datatables').find('tbody tr');

                    $container.each(function(trElemIndex, trElem) {
                        $(trElem).data('href', $table.editRoutes[trElemIndex]);
                        $(trElem).addClass('is-clickable');
                    })
                }
            },
            @endif
            columns: <?php
            echo collect($columns)->keys()
                ->map(function ($colItem) {
                    return [
                        'data' => $colItem
                    ];
                })
                ->toJson()
            ?>
        });

        // Go to edit on click
        $table.on('click', 'tr', function(e) {
            var editUrl = $(e.currentTarget).data('href');

            if (!editUrl) {
                return;
            }
            window.location = editUrl;
        })

        $('.search-input').on('keyup change', function () {
            $table.search(this.value).draw();
        })
    </script>
@endpush
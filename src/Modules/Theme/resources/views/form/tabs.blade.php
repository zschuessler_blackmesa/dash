<?php
/**
 * Form
 *
 * @see \Davinci\Dash\ViewModel\Form\Tabs\Tabs
 * @see \Davinci\Dash\ViewModel\Form
 */
?>


<div class="row">
    <form id="crud-main-form"
          method="post"
          enctype="multipart/form-data"
          action="{{ $action }}"
          class="col-12">
        @if ($method)
            @method($method)
        @endif
        {{ csrf_field() }}

        @if($content->tabs->getTabs()->count())
            <div class="component-tabs">
                <ul class="nav nav-tabs" role="tablist">
                    @foreach($content->tabs->getTabs() as $tab)
                        <li class="nav-item">
                            <a class="nav-link {{ $loop->first ? 'active' : '' }}"
                               data-toggle="tab"
                               href="#{{ $tab->slug }}">
                                {{ $tab->name }}
                            </a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($content->tabs->getTabs() as $tab)
                        <div role="tabpanel" id="{{ $tab->slug }}"
                             class="tab-pane {{ $loop->first ? 'active' : '' }}">
                            <div class="panel-body">
                                @foreach($tab->fields as $field)
                                    <div class="form-group">
                                        {{-- Label --}}
                                        <div class="form-label">
                                            <label for="{{ $field->getAttribute('name') }}">
                                                {{ $field->getProperty('label') }}
                                            </label>
                                            <div class="badges pull-right">
                                                @if ($field->getProperty('required'))
                                                    <span class="badge badge-light">required</span>
                                                @endif
                                                @if ('disabled' === $field->getAttribute('disabled'))
                                                    <span class="badge badge-light">disabled</span>
                                                @endif
                                            </div>
                                        </div>

                                        {{-- Field --}}
                                        @include($field->views->get('edit'), [
                                            'field' => $field,
                                            'form'  => $content
                                        ])
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    </form>
</div>
<?php
/**
 * Form
 *
 * @see \Davinci\Dash\ViewModel\Form
 */
?>

<div class="row">
    <form id="crud-main-form"
          method="post"
          enctype="multipart/form-data"
          action="{{ $action }}"
          class="col-12">
        @if ($method)
            @method($method)
        @endif
        {{ csrf_field() }}

            @foreach($fields as $field)
                <div class="form-group">
                    {{-- Label --}}
                    <div class="form-label">
                        <label for="{{ $field->getAttribute('name') }}">
                            {{ $field->label() }}
                        </label>
                        @if ($field->required())
                            <span class="pull-right badge badge-light">required</span>
                        @endif
                    </div>

                    {{-- Field --}}
                    @include($field->views()->get('edit'), [
                        'field' => $field,
                        'form'  => $content
                    ])
                </div>
            @endforeach
    </form>
</div>
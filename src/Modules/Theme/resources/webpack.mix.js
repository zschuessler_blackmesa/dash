const mix = require('laravel-mix');

mix.setPublicPath('../public/');

mix.js('build/js/theme.js', '')
    .js('build/js/vendor.js', '')
    .sass('build/sass/theme.scss', 'theme.css')
    .copyDirectory('build/images/', '../public/images')
    .copyDirectory('build/vendor/', '../public/vendor')

if (mix.inProduction()) {
    mix.version();
} else {
    mix.sourceMaps();
}
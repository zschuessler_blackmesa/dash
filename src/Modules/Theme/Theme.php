<?php

namespace Dash\Modules\Theme;

use Dash\Modules\Theme\Providers\AssetServiceProvider;
use Dash\Modules\Theme\Providers\MenuServiceProvider;
use Dash\Modules\Theme\Providers\ThemeServiceProvider\BodyClassGenerator;

class Theme
{
    public $title;

    public $menu;

    public $assets;

    public $bodyClassGenerator;

    public function __construct()
    {
        $this->menu = app()->make(MenuServiceProvider::class);
        $this->assets = app()->make(AssetServiceProvider::class);
        $this->bodyClasses = new BodyClassGenerator;
    }

    public function bodyClasses()
    {
        return $this->bodyClasses;
    }

    public function assets()
    {
        return $this->assets;
    }

    public function menu()
    {
        return $this->menu;
    }

    public function getRegisteredStyles()
    {
        return $this->styles;
    }

    public function assetPath($givenPath = null)
    {
        $path = sprintf('dash/theme/%s/%s', \Config::get('theme.slug'), $givenPath);
        $path = str_replace('//', '/', $path);

        return asset($path);
    }
}
<?php
/**
 * View Text Field
 *
 * Renders a simple text field
 *
 * @see \Modules\Field\Type\Text
 */
/** @var $field \Modules\Field\Type\Text */
?>
@if ($field->hasTitleThumbnail())
    @if ($thumbnailUrl = $field->getTitleThumbnailUrl())
        <img class="img-sm img-thumbnail" src="{{ $field->getTitleThumbnailUrl() }}" />
    @else
        <img class="img-sm img-thumbnail" src="{{ asset('theme/images/default-thumbnail.png') }}" />
    @endif
@endif
@if(isset($field->link))
    <a href="{{ $field->link }}">
        {!! $field->getAttribute('value') !!}
    </a>
@else
    {!! $field->getAttribute('value') !!}
@endif
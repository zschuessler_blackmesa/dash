<?php
/**
 * Virtual Field
 *
 * Renders a textbox for a text type column.
 */
?>

@include($field->content->view, $field->content->data->toArray())
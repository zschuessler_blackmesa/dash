<?php
/**
 * View Text Field
 *
 * Renders a simple text field
 */
?>

{!! $field->getAttribute('value') !!}

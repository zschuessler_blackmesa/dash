<?php
/**
 * Edit Textarea Field
 *
 * Renders a wysiwyg for a text type column.
 *
 * @var $field \Davinci\Dash\Field\Textarea
 */
?>
<textarea
@foreach($field->getAttributes()->except('value') as $attributeKey => $attributeValue)
    {{ $attributeKey }}="{!! $attributeValue !!}"
@endforeach>{!! $field->getAttribute('value') !!}</textarea>

<?php
/**
 * Edit Text Field
 *
 * Renders a textbox for a text type column.
 */
?>
<input type="text"
@foreach($field->getAttributes() as $attributeKey => $attributeValue)
    {{ $attributeKey }}="{!! $attributeValue !!}"
@endforeach
/>

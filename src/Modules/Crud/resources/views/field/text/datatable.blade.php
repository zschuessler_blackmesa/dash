<?php
/**
 * View Text Field
 *
 * Renders a simple text field
 *
 * @see \Modules\Field\Type\Text
 */
/** @var $field \Dash\Modules\Crud\Field\Text */
?>
{!! $field->getAttribute('value') !!}

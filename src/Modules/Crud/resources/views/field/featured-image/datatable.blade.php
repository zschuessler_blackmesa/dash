<?php
/**
 * View Text Field
 *
 * Renders a simple text field
 */
?>
@if(isset($field->link))
    <a href="{{ $field->link }}">
        {!! $field->getAttribute('value') !!}
    </a>
@else
    {!! $field->getAttribute('value') !!}
@endif
<?php
/**
 * Edit Text Field
 *
 * Renders a textbox for a text type column.
 *
 * @var $field \Davinci\Dash\Field\FeaturedImage
 */
?>

<div class="field-single-file-input">
    @if ($field->getAttribute('value'))
        <div class="file-preview">
            <img src="{{ \Storage::disk('public')->url($field->getAttribute('value')) }}" class="img-thumbnail"/>
        </div>
    @endif
    <div class="custom-file file-input-container" id="wrapper_{{ $field->getAttribute('name') }}">
        <input id="{{ $field->getAttribute('name') }}"
               type="file"
               accept="*"
        @foreach($field->getAttributes() as $attributeKey => $attributeValue)
            {{ $attributeKey }}="{!! $field->getAttribute($attributeKey) !!}"
        @endforeach
        >
        <label class="custom-file-label" for="{{ $field->getAttribute('name') }}">Choose file</label>
    </div>
</div>
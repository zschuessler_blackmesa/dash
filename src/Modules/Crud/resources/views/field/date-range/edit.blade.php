<?php

/**
 * Date Range: Edit
 * Renders a date range field.
 *
 * @see \Davinci\Dash\Field\DateRange
 */

/* @todo: put value logic in viewmodel */
$value = null;

if ($field->value_start && $field->value_end) {
    $value = sprintf(
        '%s - %s',
        $field->value_start->format('F jS, Y'),
        $field->value_end->format('F jS, Y')
    );
} else if ($field->value_start) {
    $value = $field->value_start->format('F jS, Y');
} else if ($field->value_end) {
    $value = $field->value_end->format('F jS, Y');
}
?>
<input type="text"
@foreach($field->getAttributes()->except('value') as $attributeKey => $attributeValue)
    {{ $attributeKey }}="{!! $attributeValue !!}"
@endforeach
/>

@push('before_body_end')
    <script>
        $('input[name="{{ $field->getAttribute('name') }}"').daterangepicker({
            locale: {
                format: 'MMMM Do, YYYY',
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            }
        })
    </script>
@endpush

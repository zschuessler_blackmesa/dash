<?php
/**
 * View Text Field
 *
 * Renders a simple text field
 */
/** @var $formattedValue \Illuminate\Support\Carbon */
$formattedValue = $field->getAttribute('value');
?>

@if($formattedValue && isset($formattedValue->timestamp))
    <span style="display:none">{{ $formattedValue->timestamp ?? '' }}</span>
    {{ $formattedValue->format('M jS, Y') }}
@endif
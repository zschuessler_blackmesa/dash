<?php
/**
 * View One to One Field
 *
 * Renders a simple text field
 *
 * @see \Modules\Field\Type\SelectOneToOne
 */
/** @var $field \Modules\Field\Type\SelectOneToOne */
?>
@if ($field->getImageUrl())
    <img src="{{ $field->getImageUrl() }}" class="img-thumbnail img-sm"/>
@endif

@if ($field->hasForeignEditRoute())
    <a href="{{ route($field->getForeignEditRouteName(), $field->foreignModel) }}" title="edit">
       {{ $field->getViewValue() }}
    </a>
@else
    {{ $field->getViewValue() }}
@endif

<?php
/**
 * Edit Select Belongs To Field
 *
 * Renders a select2 box for a belongs to many field.
 *
 * @see \Davinci\Dash\Field\SelectBelongsToMany
 */
?>
<select multiple="multiple"
    @foreach($field->getAttributes()->except('value') as $attributeKey => $attributeValue)
        {{ $attributeKey }}="{!! $attributeValue !!}"
    @endforeach
    >
    <option value="">Select {{ $field->getProperty('label') }}...</option>
</select>

@push('before_body_end')
    <script>
        $('.field-{{ $field->getAttribute('name')}}-edit').select2({
            theme: 'dash-theme',
            escapeMarkup: function (markup) {
                return markup;
            },
            @if($field->ajaxUrl)
            ajax: {
                url: '{{ $field->ajaxUrl }}',
                dataType: 'json',
                type: 'POST',
                data: function (jsonData) {
                    return $.extend({}, jsonData, {
                        _token: '{{ csrf_token() }}',
                        context: 'ajax_relational_search',
                        resource_model: "{!! urlencode(\get_class($field->getForeignResourceModel())) !!}",
                        model_class: "{!! urlencode(\get_class($field->foreignModel)) !!}"
                    });
                },
                processResults: function (response) {
                    return {
                        results: response.data
                    }
                }
            },
            templateSelection: function (d) {
                if (d.title) {
                    return d.title;
                }

                if (d.text) {
                    return d.text;
                }
            },
            templateResult: function (d) {
                if (d.title) {
                    return d.title;
                }

                if (d.text) {
                    return d.text;
                }
            },
            @endif
        });
    </script>
@endpush
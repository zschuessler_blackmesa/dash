<?php
/**
 * Media Gallery Field
 *
 * @var $field \App\Davinci\Field\MediaGallery
 *
 */
?>


<div class="form-group">
    <label for="{{ $field->getAttribute('name') }}">
        {{ $field->getProperty('label') }}
    </label>
    @if ($field->getProperty('required'))
        <span class="pull-right badge badge-light">required</span>
    @endif
    <div>
        <button type="button" class="btn btn-primary uppy-button-{{ $field->getAttribute('name') }}">Upload
            Screenshots
        </button>
    </div>
    <div class="uppy-container-{{ $field->getAttribute('name') }}" style="display: none"></div>


    {{-- Vue Component --}}
    <media-gallery-field
            ref="{{ $field->getAttribute('name') }}"
            field-name="{{ $field->getAttribute('name') }}"
            items-json="{{ $field->getAttribute('value') }}"></media-gallery-field>
</div>
<div class="hr-line-dashed"></div>

@push('before_body_end')
    <script>
        $(document).ready(function () {
            // Global Event Bus for this component
            var fieldName = "{{ $field->getAttribute('name') }}";
            var vueComponent = DashVueApp.$refs[fieldName];


            /**
             * Uppy File upload
             */
            var $uppyContainer = $('.uppy-container-{{ $field->getAttribute('name') }}');
            var $uppyButton = $('.uppy-button-{{ $field->getAttribute('name') }}');
            var uppy = Uppy.Core()
                .use(Uppy.Dashboard, {
                    target: '.uppy-container-{{ $field->getAttribute('name') }}',
                    inline: true,
                    height: 200,
                    proudlyDisplayPoweredByUppy: false
                })
                .use(Uppy.XHRUpload, {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    bundle: true,
                    autoRetry: false,
                    endpoint: '{{ route('account.extension.post-upload-screenshots', ['extension' => $field->model]) }}',
                    getResponseData(responseText, xhrResponse) {
                        $uppyButton.fadeIn();
                        $uppyContainer.fadeOut();

                        vueComponent.setItemsFromJsonString(JSON.parse(xhrResponse.response));
                    }
                });

            $uppyButton.on('click', function () {
                $uppyContainer.fadeIn();
                $uppyButton.fadeOut();
            })

            uppy.on('complete', (result) => {
                console.log('Upload complete! We’ve uploaded these files:', result.successful)
            })
        })
    </script>
@endpush

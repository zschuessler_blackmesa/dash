<?php
/**
 * Create Textarea Field
 *
 * Renders a wysiwyg for a text type column.
 *
 * @var $field \Davinci\Dash\Field\Textarea
 */
?>
<textarea
@foreach($field->getAttributes()->except('value') as $attributeKey => $attributeValue)
    {{ $attributeKey }}="{!! $field->getAttribute($attributeKey) !!}"
@endforeach>{!! $field->getAttribute('value') !!}</textarea>

@push('before_body_end')
    <script>
        $('.{{ $field->getUniqueClassIdentifier() }}').summernote({
            height: 400
        });
    </script>
@endpush
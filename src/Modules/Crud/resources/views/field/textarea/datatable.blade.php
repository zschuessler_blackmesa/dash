<?php
/**
 * View Text Field
 *
 * Renders a simple text field
 */
?>
@if(isset($field->link))
    <a href="{{ $field->link }}">
        {!! $field->data->get('value') !!}
    </a>
@else
    {!! $field->data->get('value') !!}
@endif
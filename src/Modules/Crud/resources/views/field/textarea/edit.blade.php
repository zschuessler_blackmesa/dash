<?php
/**
 * Edit Textarea Field
 *
 * Renders a wysiwyg for a text type column.
 *
 * @var $field \Davinci\Dash\Field\Textarea
 */
?>
<textarea
@foreach($field->getAttributes()->except('value') as $attributeKey => $attributeValue)
    {{ $attributeKey }}="{!! $field->getAttribute($attributeKey) !!}"
@endforeach>{!! $field->getAttribute('value') !!}</textarea>

@push('before_body_end')
    <script>
        tinymce.init({
            selector: '.{{ $field->getClassAttribute() }}',
            menubar: false,
            width: '100%'
        })
    </script>
@endpush

<?php
/**
 * Edit Textarea Field
 *
 * Renders a wysiwyg for a text type column.
 *
 * @var $field \Davinci\Dash\Field\Textarea
 */
?>
@foreach($field->fields as $childField)
    <div class="form-group">
        <div class="form-label">
            <label for="{{ $childField->getAttribute('name') }}">
                {{ $childField->getProperty('label') }}
            </label>
            @if ($childField->getProperty('required'))
                <span class="pull-right badge badge-light">required</span>
            @endif
        </div>
        @include($childField->views->get('edit'), ['field' => $childField])
    </div>
@endforeach
<?php
/**
 * Edit Select One to One Field
 *
 * Renders a select2 box for a one to one field.
 *
 * @var $field \Dash\Modules\Crud\Field\SelectOneToOne
 */
?>
<select
@foreach($field->getAttributes()->except('value') as $attributeKey => $attributeValue)
    {{ $attributeKey }}="{!! $attributeValue !!}"
@endforeach
>
@if ($field->getAttribute('value'))
    <option value="{{ $field->getAttribute('value') }}" selected="selected">
        {!! $field->getForeignResourceModel()->getSelect2OptionTextForRow($field->foreignModel) !!}
    </option>
    @endif
    </select>

    @push('before_body_end')
        <script>
            $('select[name="{{ $field->slug() }}"]').select2({
                placeholder: 'Select {{ $field->label() }}..',
                theme: 'dash-theme',
                escapeMarkup: function (markup) {
                    return markup;
                },
                @if($field->ajaxUrl)
                ajax: {
                    url: '{{ $field->ajaxUrl }}',
                    dataType: 'json',
                    type: 'POST',
                    data: function (jsonData) {
                        return $.extend({}, jsonData, {
                            _token: '{{ csrf_token() }}',
                            context: 'ajax_relational_search',
                            resource_model: "{!! urlencode(\get_class($field->getForeignResourceModel())) !!}",
                            model_class: "{!! urlencode(\get_class($field->foreignModel)) !!}"
                        });
                    },
                },
                templateResult: function (d) {
                    return d.text;
                },
                templateSelection: function (d) {
                    return d.text;
                },
                @endif
            });
        </script>
    @endpush

<?php
/**
 * Crud Index Page
 *
 * @var $model mixed The model object.
 * @var $datatable
 * @var $routes
 * @var $labels
 */
?>

@extends('theme::layouts.default')

@section('content')
    {{-- Page Title --}}
    <div class="row middle-xs between-xs crud-page-title">
        <div class="p-15">
            <div class="col-6">
                <h1>
                    {{ $labels->get('plural') }}
                </h1>
            </div>
        </div>
        <div class="col-6">
            <a href="{{ route($routes->get('create')) }}" class="button button-primary pull-right has-icon-right">
                Create {{ $labels->get('singular') }}
                <span class="fa fa-plus"></span>
            </a>
        </div>
    </div>

    {{-- Page Content --}}
    <div class="row animated fadeIn">

        <div class="col-12">
            @if(isset($content) && $content instanceof \Illuminate\Support\Collection)
                @foreach($content as $content)
                    @include($content->view, $content->data)
                @endforeach
            @else
                @include($content->view, $content->data)
            @endif
        </div>
    </div>
@endsection
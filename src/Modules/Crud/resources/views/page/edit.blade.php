<?php
/**
 * Crud Edit Page
 *
 * @var $model mixed The model object.
 * @var $tableRows \Illuminate\Support\Collection
 *
 * @see \Dash\Modules\Crud\Http\Controllers\CrudController::edit()
 */
?>

@extends('theme::layouts.default')

@section('content')
    {{-- Page Title --}}
    <div class="row middle-xs between-xs crud-page-title">
            <div class="col-6">
                <div class="p-15">
                <h1>
                    Edit {{ $labels->get('singular') }}
                </h1>
                <div class="component-breadcrumb p-t-10 p-b-10">
                    <a class="breadcrumb-item" href="{{ route(config('dash.default_route_namespace')) }}">Home</a>
                    <a class="breadcrumb-item" href="{{ route($routes->get('index')) }}">
                        {{ $labels->get('plural') }}
                    </a>
                    <span class="breadcrumb-item">Edit</span>
                </div>
                </div>
            </div>
        <div class="col-6">
            <div class="crud-form-submit pull-right p-15">
                <button type="button" class="btn-submit-form button buttom-primary is-clickable" data-target="#crud-main-form">
                    {{ 'Update ' . $labels->get('singular') }}
                </button>
            </div>
        </div>
    </div>

    {{-- Page Content --}}
    <div class="row animated fadeIn">
        <div class="col-12">
            @if(isset($content) && $content instanceof \Illuminate\Support\Collection)
                @foreach($content as $content)
                    @include($content->view, $content->data)
                @endforeach
            @else
                @include($content->view, $content->data)
            @endif
        </div>
    </div>
@endsection

@push('before_body_end')
    <script>
        (function () {
            var $submitBtn = $('.btn-submit-form');

            $submitBtn.on('click', function () {
                var $this = $(this);

                $this.attr('disabled', 'disabled');

                var $form = $($this.data('target'));
                $form.submit();
            });
        })();
    </script>
@endpush
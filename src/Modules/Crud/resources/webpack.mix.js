const mix = require('laravel-mix');

mix.setPublicPath('../public/');

mix.js('build/js/crud.js', '')
    .js('build/js/vendor.js', '')
    .sass('build/sass/crud.scss', 'crud.css')
    .copyDirectory('build/images/', '../public/images');

if (mix.inProduction()) {
    mix.version();
} else {
    mix.sourceMaps();
}
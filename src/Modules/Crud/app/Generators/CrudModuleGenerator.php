<?php

namespace Dash\Modules\Crud\Generators;

use Dash\Module\Generators\ModuleGenerator;
use Illuminate\Support\Str;

class CrudModuleGenerator extends ModuleGenerator
{

    public function __construct($options = null)
    {
        parent::__construct($options);

        $this->basePath = data_get($options, 'basePath') ??  sprintf('%s/%s', config('dash-crud.path'), $this->name);

        $this->module = $this->getDefaultModuleOptions()
            ->merge([
                'crud' => [
                    'autoloadTarget' => $this->getCrudAutoloadPath(),
                    'model_name' => $this->getCrudModelName(),
                    'model_include' => $this->getCrudModelInclude(),
                    'model_policyname' => $this->getCrudModelPolicyName()
                ]
            ]);
    }

    public function getCrudModelPolicyName()
    {
        return $this->getCrudModelName() . 'Policy';
    }

    public function getCrudModelName()
    {
        return Str::studly($this->name);
    }

    public function getCrudModelInclude()
    {
        $path = config('dash.default_model_path');

        // Convert all folder names to uppercase; swap slash type
        $path = collect(explode('/', $path))
            ->transform(function($folderPath) {
                return ucwords($folderPath);
            })
            ->implode('\\');

        // Add Slash to end if it doenst exist
        $path =  rtrim($path, '\\') . '\\';

        // Add model name
        $path .= $this->getCrudModelName();

        return $path;
    }

    public function getCrudAutoloadPath()
    {
        /**
         * Namespace is given in this form:
         * App\Modules\Crud
         *
         * It needs to match the autoload target:
         * Modules/Crud/{$moduleName}/app
         */
        $namespace = config('dash-crud.namespace') . '/' . $this->name;
        $namespace = str_replace('App\\', '', $namespace);
        $namespace = str_replace('\\', '/', $namespace);
        $namespace .= '/app';

        return $namespace;
    }

    public function getNamespaceFromConfig()
    {
        return config('dash-crud.namespace');
    }

    public function generate()
    {

        if (!$this->name) {
            throw new \Exception('A module name is required to generate a new module.');
        }

        if (!$this->templatesPath) {
            $this->templatesPath = __DIR__ . '/module-template';
        }

        $this->module = $this->getDefaultModuleOptions()
            ->merge(collect($this->module));

        if (!$this->basePath) {
            $this->basePath = sprintf('%s/Crud/%s', config('dash.modules.path'), $this->name);
        }

        // Assert module can be created
        $this->assertCanCreateModule();

        // Create all files from stubs
        $this->createModuleDirectories();
        $this->createModuleFiles();

        return $this;
    }
}

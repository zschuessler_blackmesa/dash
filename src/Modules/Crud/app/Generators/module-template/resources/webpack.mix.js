const mix = require('laravel-mix');

mix.setPublicPath('../public/');

mix.js('build/js/$MODULE_SLUG$.js', '')
    .js('build/js/vendor.js', '')
    .sass('build/sass/$MODULE_SLUG$.scss', '$MODULE_SLUG$.css')
    .copyDirectory('build/images/', '../public/images')
    .copyDirectory('build/vendor/', '../public/vendor')

if (mix.inProduction()) {
    mix.version();
} else {
    mix.sourceMaps();
}
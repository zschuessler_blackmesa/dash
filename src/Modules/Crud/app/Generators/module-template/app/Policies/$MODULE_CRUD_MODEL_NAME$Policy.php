<?php

namespace $MODULE_CLASSES_NAMESPACE$;

use Illuminate\Auth\Access\HandlesAuthorization;
use $MODULE_CRUD_MODEL_INCLUDE$;
use App\User;

class $MODULE_CLASSES_NAME$
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User $user
     * @param  $MODULE_CRUD_MODEL_NAME$  $model
     * @return mixed
     */
    public function view(User $user, $MODULE_CRUD_MODEL_NAME$ $model)
    {
        return true;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User $user
     * @param  $MODULE_CRUD_MODEL_NAME$  $model
     * @return mixed
     */
    public function update(User $user, $MODULE_CRUD_MODEL_NAME$$model)
    {
        return true;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User $user
     * @param  $MODULE_CRUD_MODEL_NAME$  $model
     * @return mixed
     */
    public function delete(User $user, $MODULE_CRUD_MODEL_NAME$ $model)
    {
        return true;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User $user
     * @param  $MODULE_CRUD_MODEL_NAME$  $model
     * @return mixed
     */
    public function restore(User $user, $MODULE_CRUD_MODEL_NAME$ $model)
    {
        return true;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User $user
     * @param  $MODULE_CRUD_MODEL_NAME$  $model
     * @return mixed
     */
    public function forceDelete(User $user, $MODULE_CRUD_MODEL_NAME$ $model)
    {
        return true;
    }
}

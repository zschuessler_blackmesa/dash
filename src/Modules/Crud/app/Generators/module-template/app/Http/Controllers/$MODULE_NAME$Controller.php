<?php

namespace $MODULE_CLASSES_NAMESPACE$;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Dash\Modules\Crud\Http\Controllers\CrudController;

use $MODULE_CRUD_MODEL_INCLUDE$;

class $MODULE_CLASSES_NAME$ extends CrudController
{
    public $modelClass = $MODULE_CRUD_MODEL_NAME$::class;
}
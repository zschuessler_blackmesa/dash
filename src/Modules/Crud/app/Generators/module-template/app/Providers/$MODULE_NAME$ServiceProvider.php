<?php

namespace $MODULE_NAMESPACE$\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use $MODULE_CRUD_MODEL_INCLUDE$;
use $MODULE_NAMESPACE$\Policies\$MODULE_CRUD_MODEL_POLICYNAME$;

class $MODULE_NAME$ServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');
        $this->registerPolicies();
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../../config/dash-crud-$MODULE_SLUG$.php' => config_path('dash-crud-$MODULE_SLUG$.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../../config/dash-crud-$MODULE_SLUG$.php', 'dash-crud-$MODULE_SLUG$'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/$MODULE_SLUG$');

        $sourcePath = __DIR__.'/../../resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/$MODULE_SLUG$-module';
        }, \Config::get('view.paths')), [$sourcePath]), '$MODULE_SLUG$');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/$MODULE_SLUG$');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, '$MODULE_SLUG$');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../../resources/lang', '$MODULE_SLUG$');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../../database/factories');
        }
    }

    public function registerPolicies()
    {
        \Gate::policy($MODULE_CRUD_MODEL_NAME$::class, $MODULE_CRUD_MODEL_POLICYNAME$::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}

<?php

/*
|--------------------------------------------------------------------------
| $MODULE_NAME$ Crud Resource Routes
|--------------------------------------------------------------------------
|
| Default resource routes generated for $MODULE_NAME$
|
*/

Route::group([
    'middleware' => $MODULE_DASH_MIDDLEWARE$,
        'as'     => '$MODULE_DASH_ROUTENAMESPACE$.',
        'prefix' => '$MODULE_DASH_ROUTENAMESPACE$'
    ],
    function () {
        // $MODULE_NAME$ Resource Route
        Route::resource('$MODULE_SLUGPLURAL$', '$MODULE_NAME$Controller');
    }
);

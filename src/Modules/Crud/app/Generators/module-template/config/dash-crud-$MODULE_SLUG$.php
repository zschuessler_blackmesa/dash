<?php

return [
    'name' => '$MODULE_NAME$',
    'labels' => [
        'singular' => '$MODULE_TITLE$',
        'plural'   => '$MODULE_TITLEPLURAL$'
    ],
    'dash-menu' => [
        'title'    => '$MODULE_TITLEPLURAL$',
        'visible'  => true,
        'icon'     => 'fa fa-cubes',
        'priority' => 100
    ],
    'dash-media-gallery' => [
        'enabled' => false,
        'folder'  => '$MODULE_SLUG$',
        'driver'  => 'local'
    ]
];

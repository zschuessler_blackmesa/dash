<?php

namespace Dash\Modules\Crud;

class ViewModel
{
    public $view;

    public $data;

    public function __construct()
    {
        $this->data = collect($this->data)->mapWithKeys(function($key, $value) {
            return [$key => null];
        });
    }

    public function fill($fillArray)
    {
        foreach ($fillArray as $fillKey => $fillValue) {
            $this->data->put($fillKey, $fillValue);
        }

        return $this;
    }

    public function createView()
    {
        $viewData = $this->data->toArray();

        foreach ($viewData as $viewKey => &$viewValue) {
            if (is_array($viewValue)) {
                $viewValue = collect($viewValue);
            }
        }

        return view($this->view)
            ->with($viewData);
    }

    public function __call($name, $arguments)
    {
        if ($this->data->has($name)) {
            $value = $arguments[0];

            if (is_array($value) && count($value) > 1) {
                $this->data->put($name, collect($arguments[0]));
            } else {
                $this->data->put($name, $arguments[0]);
            }

        }

        return $this;
    }

    public function __toString()
    {
        return $this->createView()->render();
    }

    public function render()
    {
        return $this->createView()->render();
    }
}
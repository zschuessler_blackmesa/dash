<?php

namespace Dash\Modules\Crud;

use Dash\Modules\Crud\Field\Virtual;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Dash\Modules\Crud\SchemaMapper;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

use Dash\Modules\Crud\ViewModel\Datatable;

class ResourceModel
{
    /**
     * @var $model Model
     */
    public $model;

    /**
     * @var $fields Collection
     */
    public $fields;

    /**
     * @var $config Collection
     */
    public $config;

    /**
     * @var $views Collection
     */
    public $views;

    /**
     * @var $labels Collection
     */
    public $labels;

    /**
     * @var $contexts Collection
     */
    public $contexts;

    /**
     * @var $routes Collection
     */
    public $routes;

    public function __construct(Model $model)
    {
        $this->model = $model;

        /**
         * Each property merges from the extending class if it's set.
         *
         * Use cases:
         *
         * 1. I want to change a single edit route. Add this to the extending class:
         *    public $routes = ['edit' => 'my.new.route.name']
         *
         * 2. I want to change ALL views. Either update the property or extend `getDefaultViews`
         */
        $this->config = collect(config('dash'))
            ->merge(collect($this->config));

        $this->routes = $this->getDefaultRoutes()
            ->merge(collect($this->routes));

        $this->views = $this->getDefaultViews()
            ->merge(collect($this->views));

        $this->labels = $this->getDefaultLabels()
            ->merge(collect($this->labels));

        $this->contexts = $this->getDefaultContexts()
            ->merge(collect($this->contexts));

        // Fields do not merge automatically
        $this->fields = collect();
    }

    /**
     * Autoload From Model
     *
     * Loads the ResourceModel from known paths, or creates a default ResourceModel if none found.
     *
     * @param Model $model
     * @return mixed
     */
    public static function autoloadFromModel(Model $model) : ResourceModel
    {
        // Match path: App\Dash\ResourceModels\{ModelName}.php
        $className = sprintf('App\Dash\ResourceModels\%s', studly_case(str_singular($model->getTable())));
        if (class_exists($className)) {
            return (new $className($model));
        }

        // Check if there is a default Dash resource model class
        $className = sprintf('Dash\ResourceModel\%s', studly_case(str_singular($model->getTable())));
        if (class_exists($className)) {
            return (new $className($model));
        }

        return (new ResourceModel($model));
    }

    public function getDefaultContexts() : Collection
    {
        return collect(['edit', 'create', 'view', 'datatable', 'ajax']);
    }

    public function getModel()
    {
        if ($this->model) {
            return $this->model;
        }
    }

    public function getFields($context = null) : Collection
    {
        if ($this->fields->count()) {
            return $this->fields;
        }

        $this->fields = $this->getDefaultFields();

        return $this->fields;
    }

    public function getDefaultFields($context = null) : Collection
    {
        $fields = (new SchemaMapper)
            ->autoloadSchemaFieldClasses($this->model);

        // Filter fields: don't included guarded attributes
        $model = $this->model;

        if ($model) {
            // Set default field context if none is set
            $fields = $fields->transform(function ($field) use ($context) {
                if (!$field->context() && $context) {
                    $field->context($context);
                }

                return $field;
            });

            // Filter guarded columns and any meta columns
            $metaColumns = ['created_at', 'deleted_at', 'updated_at'];
            $fields = $fields->filter(function ($field) use ($model, $metaColumns) {
                if ('edit' === $field->context()
                    || 'create' === $field->context()) {
                    $guardedFields = collect($model->getGuarded());
                    if ($guardedFields->contains($field->getAttribute('name'))
                        || in_array($field->getAttribute('name'), $metaColumns)) {
                            return;
                        }
                }

                return true;
            });

            // Pass each field through a method filter if it exists
            $fields = $fields->transform(function ($field) use ($context) {
                $methodName = Str::camel($field->getAttribute('name'));

                if (method_exists($this, $methodName)) {
                    return $this->$methodName($field, $context);
                }

                return $field;
            });
        }

        return $fields;
    }

    public function getFieldsForContext(string $context) : Collection
    {
        $contextMethodName = sprintf('%sFields', $context);

        if (method_exists($this, $contextMethodName)) {
            return $this->$contextMethodName();
        }
        
        return $this->getDefaultFields($context);
    }

    public function createFields() : Collection
    {
        $fields = $this->getFields('create')
            ->transform(function(Field $field) {
                $field->context('create');
                return $field;
            })
            ->reject(function(Field $field) {
                if ($field->doctrine()->column->getAutoIncrement()) {
                    return true;
                }
            });

        return $fields;
    }

    public function editFields() : Collection
    {
        $fields = $this->getFields()
            ->transform(function(Field $field) {
                $field->context('edit');
                return $field;
            })
            ->reject(function(Field $field) {
                // No autoincrement fields
                if ($field->doctrine()->column->getAutoIncrement()) {
                    return true;
                }

                // No meta columns
                if (in_array($field->getAttribute('name'), ['deleted_at', 'created_at', 'updated_at'])) {
                    return true;
                }
            });

        return $fields;
    }

    public function getDatatable() : Datatable
    {
        return $this->getDefaultDatatable();
    }

    public function getDefaultDatatable() : Datatable
    {
        $model      = $this->getModel();
        $modelTitle = $this->labels->get('singular');
        $fields     = $this->datatableFields();

        $datatable = (new Datatable)
            ->title($modelTitle)
            ->fields($fields)
            ->model($model)
            ->query($model::select()->limit(10))
            ->ajaxSearchRoute(route($this->routes->get('search')))
            ->searchText(sprintf('Search %s...', $this->labels->get('plural')))
            ->linkField($this->config->get('default_table_name_column'), $this->routes->get('edit'))
            ->generate();

        return $datatable;
    }

    public function getDefaultLabels() : Collection
    {
        $tableName = $this->model->getTable();

        return collect([
            'singular' => ucwords(str_singular(str_replace('_', ' ', $tableName))),
            'plural'   => ucwords(str_plural(str_replace('_', ' ', $tableName))),
        ]);
    }

    public function getDefaultViews() : Collection
    {
        return collect([
            'index'  => 'crud::page.index',
            'edit'   => 'crud::page.edit',
            'create' => 'crud::page.create',
            'show'   => 'crud::page.show',
            'select2_option' => 'crud::field.select2-option'
        ]);
    }

    public function getDefaultRoutes() : Collection
    {
        $defaultRouteNamespace = $this->config->get('default_route_namespace');
        $tableNameSlug         = str_slug($this->model->getTable());

        return collect([
            'index'  => sprintf('%s.%s.index', $defaultRouteNamespace, $tableNameSlug),
            'edit'   => sprintf('%s.%s.edit', $defaultRouteNamespace, $tableNameSlug),
            'create' => sprintf('%s.%s.create', $defaultRouteNamespace, $tableNameSlug),
            'store'  => sprintf('%s.%s.store', $defaultRouteNamespace, $tableNameSlug),
            'update' => sprintf('%s.%s.update', $defaultRouteNamespace, $tableNameSlug),
            'show'   => sprintf('%s.%s.show', $defaultRouteNamespace, $tableNameSlug),
            'delete' => sprintf('%s.%s.delete', $defaultRouteNamespace, $tableNameSlug),
            'search' => 'dash.ajax-service',
        ]);
    }

    public function datatableFields() : Collection
    {
        /** @var $modelColumns Collection */
        $modelColumns = collect(Schema::getColumnListing($this->model->getTable()));

        /**
         * Infer default columns to render
         *
         * By default, we looked for the default column name specified for:
         * 1. ID column (default: `id`)
         * 2. Name column (default: `name`)
         * 3. Last Updated At column (default: `updated_at`)
         * 4. Created Date column (default: `created_at`)
         */
        $defaultColumns    = collect();
        $defaultIdColumn   = $this->config->get('default_table_id_column');
        $defaultNameColumn = $this->config->get('default_table_name_column');
        $defaultImageColumn = $this->config->get('default_table_image_column');

        if ($modelColumns->contains($defaultIdColumn)) {
            $defaultColumns->put($defaultIdColumn, 'ID');
        }

        if ($modelColumns->contains($defaultNameColumn)) {
            // Show name & image if both exist, otherwise just the name field
            if ($modelColumns->contains($defaultImageColumn)) {

                /**
                 * @todo: convert this to its own title field lass
                 */
                $virtualField = (new Virtual)
                    ->model($this->model)
                    ->setRenderMethod(function(Field $field, $viewKey) use ($defaultNameColumn, $defaultImageColumn) {
                        $model = $field->model;

                        $nameFieldValue = $field->model->$defaultNameColumn;
                        $imageFieldValue = $field->model->$defaultImageColumn;
                        $filePath = \Storage::disk('public')->url($imageFieldValue);

                        $html = sprintf(
                            '<div class="row middle-xs"><img src="%s" class="component-thumbnail for-datatable p-r-10"/> <a href="%s">%s</a></div>',
                            $filePath,
                            route($this->routes->get('edit'), $model),
                            $field->model->$defaultNameColumn
                        );
                       return $html;

                    });


                $defaultColumns->put($defaultNameColumn, $virtualField);

            } else {
                $defaultColumns->put($defaultNameColumn, ucwords($defaultNameColumn));
            }

        }

        if ($modelColumns->contains('updated_at')) {
            $defaultColumns->put('updated_at', 'Last Updated');
        }

        if ($modelColumns->contains('created_at')) {
            $defaultColumns->put('created_at', 'Created');
        }

        $defaultFieldKeys = $defaultColumns->keys()->toArray();
        $fields = $this->getFields()
            ->filter(function(Field $field) use ($defaultFieldKeys) {
                return in_array($field->getAttribute('name'), $defaultFieldKeys);
            });

        return $fields;
    }

    public function getTitleField()
    {

        // Has title field?
        $fields = $this->getFields();

        $defaultNameField = $this->config->get('default_table_name_column');
        $defaultIdField = $this->config->get('default_table_id_column');

        $titleFieldIndex = $fields->search(function($field) use($defaultNameField) {
            return $field->name === $defaultNameField;
        });

        if (!$titleFieldIndex) {
            $titleFieldIndex = $fields->search(function($field) use($defaultIdField) {
                return $field->name === $defaultIdField;
            });
        }

        if (false === $titleFieldIndex) {
            throw new \Exception(sprintf(
                'Resource model was unable to determine default name field for model `%s`',
                $this->getModel()->getTable()
            ));
        }

        return $fields->get($titleFieldIndex);
    }

    public function getImageField()
    {
        // Has title field?
        $fields = $this->getFields();

        $defaultField = $this->config->get('default_table_image_column');

        $imageField = $fields->where('name', $defaultField);

        if (!$imageField->count()) {
            return null;
        }

        return $imageField->first();
    }

    public function respondToSelect2AjaxRequest(Request $request)
    {
        $titleField = $this->getTitleField();
        $model      = $this->getModel()->select();

        if ($request->input('term')) {
            $model->where($titleField->name,
                'like',
                '%' . strtolower($request->input('term')) . '%'
            );
        }

        $modelRows = $model->limit(10)->get()
            ->transform(function($row) {
                return (object)[
                    'id'   => $row->id,
                    'text' => $this->getSelect2OptionTextForRow($row)
                ];
            })
            ->toArray();

        return response()->json((object)[
            'results' => $modelRows,
            'pagination' => (object)[
                'more' => false
            ],
        ]);
    }

    public function getSelect2OptionTextForRow(Model $model)
    {
        $titleField = $this->getTitleField();
        $imageField = $this->getImageField();
        $fieldColumnName = $titleField->name;

        $html = $model->$fieldColumnName;

        /**
         * @todo: handle getting the url via some kind of intermediary service for storage
         */
        if ($imageField) {
            $imageFieldName = $imageField->name;
            $imagePath = $model->$imageFieldName;
            $filePath = \Storage::disk('public')->url($imagePath);

            $html = sprintf(
                '<img src="%s" class="select2-image-field"/> %s',
                $filePath,
                $html
            );
        }

        return view($this->views->get('select2_option'), [
            'html' => $html
        ])->render();
    }

    public function saveModel(Request $request)
    {
        $input = collect($request->input());
        $model = $this->getModel();

        /**
         * Save primary model
         */
        $attributesToSave = collect($request->except([
                '_method', '_token', 'relation_fields'
            ]))
            ->each(function($attributeValue, $attributeKey) use (&$model) {
                $model->{$attributeKey} = $attributeValue;
            });

        $model->save();

        /**
         * Save any relational data
         */
        $relationalData = collect($input->get('relation_fields'));
        if ($relationalData->count()) {
            $relationalData->each(function($relationData, $relationMethod) use ($model, $request) {
                $foreignRequest = (clone $request)->replace($relationData);
                $foreignRow = $model->$relationMethod;

                $foreignRequestFiles = data_get($foreignRequest->allFiles(), 'relation_fields.' . $relationMethod);
                if ($foreignRequest->files->count()) {
                    $foreignRequest->files->replace($foreignRequestFiles);
                }

                // Save
                if ($foreignRow) {
                    $foreignResourceModel = ResourceModel::autoloadFromModel($foreignRow);
                    $foreignResourceModel->saveModel($foreignRequest);
                } else {
                    /** @var $foreignRow \Illuminate\Database\Eloquent\Model */
                    $foreignRow = $model->$relationMethod()->getRelated()->replicate();
                    $foreignRow
                        ->fill($relationData)
                        ->save();

                    $model->$relationMethod()
                        ->associate($foreignRow)
                        ->save();
                }
            });
        }

        $this->model = $model;

        return $this;
    }
}

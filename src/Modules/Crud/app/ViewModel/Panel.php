<?php

namespace Dash\Modules\Crud\ViewModel;

use Dash\Modules\Crud\ViewModel;

class Panel extends ViewModel
{
    public $view = 'theme::panel.default';

    public $data = ['title', 'subTitle', 'content'];
}
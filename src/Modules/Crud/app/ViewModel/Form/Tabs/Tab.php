<?php

namespace Dash\Modules\Crud\ViewModel\Form\Tabs;

use Dash\Modules\Crud\ViewModel;

class Tab extends ViewModel
{
    public $name;

    public $slug;

    public $fields;

    public $view = 'theme::form.tabs.tab';

    public function __construct($tabName, $fields = null)
    {
        $this->name = $tabName;
        $this->slug = str_slug($tabName);
        $this->fields = $fields;
    }
}
<?php

namespace Dash\Modules\Crud\ViewModel\Form\Tabs;

use Illuminate\Support\Collection;
use Dash\Modules\Crud\SchemaMapper;
use Dash\Modules\Crud\ViewModel;

class Tabs extends ViewModel
{
    public $view = 'theme::form.tabs';

    public $tabs;

    public function __construct()
    {
        parent::__construct();

        $this->tabs = collect();
    }

    public function createTab($tabName, $fields = null)
    {
        $this->tabs->put($tabName, new Tab($tabName, $fields));

        return $this;
    }

    public function getTabs()
    {
        return $this->tabs;
    }
}
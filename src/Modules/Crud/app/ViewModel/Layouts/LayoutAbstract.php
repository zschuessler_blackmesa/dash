<?php

namespace Dash\Modules\Crud\ViewModel\Layouts;

use Dash\Modules\Crud\ResourceModel;
use Dash\Modules\Crud\ViewModel;
use Illuminate\Support\Collection;

class LayoutAbstract extends ViewModel
{
    /**
     * @var ResourceModel
     */
    public $resourceModel;

    public $model;

    public $labels;

    public $routes;

    /**
     * @var Collection A collection of view data to supply the Blade template.
     */
    public $data;

    public $content;
}
<?php

namespace Dash\Modules\Crud\ViewModel\Layouts\Crud;

use Dash\Modules\Crud\ResourceModel;
use Dash\Modules\Crud\ViewModel;
use Illuminate\Support\Collection;

class DefaultLayout extends ViewModel\Layouts\LayoutAbstract
{
    public $context;

    public function getContext()
    {
        if ($this->context) {
            return $this->context;
        }

        $requestPath = request()->path();

        if (ends_with($requestPath, '/create')) {
            return 'create';
        }

        if (ends_with($requestPath, '/edit')) {
            return 'edit';
        }
    }

    public function getDefaultLayout() : ViewModel
    {
        $resourceModel = $this->resourceModel;
        $model = $this->model;
        $editableFields = $resourceModel->getFieldsForContext($this->getContext());

        $form = (new ViewModel\Form())
            ->action(route($resourceModel->routes->get('update'), [$model]))
            ->context($this->context)
            ->method($this->getFormMethod())
            ->model($model)
            ->fields($editableFields);

        $formPanel = (new ViewModel\Panel())
            ->content($form);

        return $formPanel;
    }

    public function getFormMethod()
    {
        if ('create' === $this->getContext()) {
            return 'post';
        }
        if ('edit' === $this->getContext()) {
            return 'put';
        }
    }

    public function setResourceModel(ResourceModel $resourceModel)
    {
        $this->resourceModel = $resourceModel;

        $this->model = $resourceModel->getModel();

        $this->labels = $resourceModel->labels;

        $this->routes = $resourceModel->routes;

        $this->context = $this->getcontext();

        $this->view = $resourceModel->views->get($this->context);

        return $this;
    }

    public function createView()
    {
        if (!$this->content) {
            $this->content = $this->getDefaultLayout();
        }

        $view = parent::createView();

        return $view->with([
            'content' => $this->content,
            'model' => $this->model,
            'labels' => $this->labels,
            'routes' => $this->routes,
            'resourceModel' => $this->resourceModel
        ]);
    }
}
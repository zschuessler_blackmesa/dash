<?php

namespace Dash\Modules\Crud\ViewModel;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Dash\Modules\Crud\SchemaMapper;
use Dash\Modules\Crud\ViewModel;
use Dash\Modules\Crud\ViewModel\Form\Tabs\Tabs;

class Form extends ViewModel
{
    public $view = 'theme::form.default';

    public $tabs;

    public $context;

    public $data = ['title', 'subTitle', 'fields', 'model', 'action', 'buttonText', 'method', 'layout', 'context'];

    public function __construct()
    {
        parent::__construct();

        $this->tabs = new Tabs;
    }

    public function context($context)
    {
        $this->data->put('context', $context);

        return $this;
    }

    public function method($method)
    {
        $this->data->put('method', strtoupper($method));

        return $this;
    }

    public function layout($layoutType)
    {
        $this->view = 'theme::form.' . $layoutType;

        return $this;
    }
    
    public function createTab($name = 'Unnamed Tab', $fields = null)
    {
        if (is_callable($fields)) {
            $fields = call_user_func($fields);
        }

        $this->tabs->createTab($name, $fields);

        return $this;
    }

    public function fields($fields)
    {
        /** @var $fields Collection */
        $fields = collect($fields);

        $this->data->put('fields', $fields);

        return $this;
    }
}
<?php

namespace Dash\Modules\Crud\ViewModel;

class Field
{
    public $name;

    public $views;

    public $view = 'theme::field.text';

    public $data = ['label', 'name', 'value'];

    public function __construct()
    {
        $nameSlug = str_slug($this->name);

        $this->views = collect([
            'view'   => sprintf('theme::form.field.%s.view', $nameSlug),
            'edit'   => sprintf('theme::form.field.%s.edit', $nameSlug),
            'create' => sprintf('theme::form.field.%s.create', $nameSlug),
        ]);
    }

    public function view()
    {
        return view($this->view->get('view'));
    }

    public function edit()
    {
        return view($this->view->get('edit'));
    }

    public function create()
    {
        return view($this->view->get('create'));
    }
}
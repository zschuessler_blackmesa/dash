<?php

namespace Dash\Modules\Crud\ViewModel;

use Dash\Modules\Crud\ResourceModel;
use Doctrine\DBAL\Schema\Column;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Dash\Modules\Crud\SchemaMapper;
use Dash\Modules\Crud\ViewModel;
use Dash\Modules\Crud\Field;

class Datatable extends ViewModel
{
    public $data = ['title', 'columns', 'ajaxSearchRoute', 'searchText', 'linkField', 'rows'];

    public $view = 'theme::datatable.default';

    public $model;

    public $rows;

    public $fields;

    public $queryResponse;

    public function linkField($columnName, $routeName)
    {
        $this->data->put('linkField', (object)[
            'columnName' => $columnName,
            'routeName'  => $routeName
        ]);

        // If no model set, no link field can be auto-generated
        if (!$this->model) {
            return $this;
        }

        // Set link field automatically
        $rows = $this->data->get('rows');
        if ($rows && collect($rows)->count()) {
            $rows->transform(function($row) {
                $row->each(function(&$field) use ($row) {
                    $linkField = $this->data->get('linkField');

                    if ($field->data->get('name') === $linkField->columnName) {
                        $field->link = route($linkField->routeName, $field->model);
                    }
                });

                return $row;
            });

            $this->data->put('rows', $rows);
        }

        return $this;
    }

    public function query($value)
    {
        if (Builder::class === get_class($value)) {
            /** @var $value Builder */

            if (!$this->data->get('model')) {
                $this->data->put('model', $value->getModel());
            }

            $this->queryResponse = $value->get();
        }

        return $this;
    }

    public function fields($fieldCollection)
    {
        $this->fields = collect($fieldCollection);

        return $this;
    }

    public function generate()
    {
        $modelRows = collect($this->queryResponse);
        $fields = collect($this->fields);

        // Set columns
        $columns = $fields->mapWithKeys(function(Field $field) {
            return [
                $field->getAttribute('name') => $field->label()
            ];
        });
        $this->data->put('columns', $columns);

        // Set rows
        /**
         * @todo: there is an unresolved bug where, without resetting the attributes property
         *        below, nothing changes for the object and it is cached instead (all values remain the same).
         *        Quite strange! This happened for the model too until using clone.
         */
        $tableRows = $modelRows->map(function(Model $modelRow)  {
            $rowFields = $this->fields->map(function(Field $field) use ($modelRow) {
                $newField = clone $field;
                $newField->model($modelRow);
                $newField->setRawAttributes([]);
                $newField->setAttribute('value', $modelRow->getAttribute($field->getAttribute('name')));

                return $newField;
            });

            return $rowFields;
        });

        $this->data->put('rows', $tableRows);

        return $this;
    }
}
<?php

namespace Dash\Modules\Crud\ViewModel;

use Dash\Modules\Crud\ViewModel;

class Page extends ViewModel
{
    public $view = 'theme::page.default';

    public $data = ['title', 'subTitle', 'content'];
}
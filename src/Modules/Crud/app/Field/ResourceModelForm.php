<?php

namespace Dash\Modules\Crud\Field;

use Dash\Modules\Crud\Field;
use Dash\Modules\Crud\ResourceModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\Relation;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class ResourceModelForm
 *
 * A field that acts like an entire resource model form.
 *
 * Example use case:
 *
 * Model: User
 * Column: profile_id
 *
 * You want the user to edit their profile without visiting a new form and page.
 * Make the `profile_id` field a ResourceModelForm, which will automatically scaffold a form based on the
 * foreign model.
 *
 * @package Davinci\Dash\Field
 */
class ResourceModelForm extends Field
{
    public $fields;

    public $resourceModel;

    public $parentField;

    public $formType = 'default';

    public $formTypes = [
        // default: a standard HTML form
        'default',
        // modal: form loads in a modal
        'ajax',
        // child: outputs only fields, namespaced by the model table name
        'child'
    ];

    /**
     * @var Relation
     */
    public $relation;

    public $required = false;


    public function __construct()
    {
        parent::__construct();

        $this->fields = collect();

        $this->dispatcher->addListener('field.createdByMorph', [$this, 'onCreatedByMorph']);
        $this->dispatcher->addListener('field.formTypeSet', [$this, 'onFormTypeSet']);
    }

    /**
     * Event: Created By Morph
     *
     * If this field was created by being morphed through the `morphType` method, we should auto-load
     * all of the form child fields by default for the user.
     *
     * @param $event
     */
    public function onCreatedByMorph($event)
    {
        $oldField = $event->oldField;
        $newField = $event->newField;

        if (!$oldField->foreignKey) {
            return;
        }

        // Add reference to old field
        $this->parentField = $oldField;

        // Update model
        $this->model = $oldField->foreignModel;
    }

    public function setFormType($type)
    {
        if (!in_array($type, $this->formTypes)) {
            throw new \Exception('Invalid form type: ' . $type);
        }

        $this->formType = $type;

        // Relational fields
        $event = new Event();
        $event->formType = $type;
        $this->dispatcher->dispatch('field.formTypeSet', $event);

        return $this;
    }

    public function onFormTypeSet($event)
    {
        $formType = $event->formType;

        /**
         * Relation
         */
        if ('child' === $formType) {
            if (!$this->relation) {
                throw new \Exception('Child forms must set the relation before setting the for type. See `setRelation`');
            }

            /**
             * Namespace fields with {table_name}[]{field_name]
             *
             * This prevents issues with the form being a child of another form.
             */
            if (BelongsTo::class === get_class($this->relation)) {
                $this->fields->transform(function (Field $field) {
                    $nameField = sprintf(
                        'relation_fields[%s][%s]',
                        $this->relation->getRelation(),
                        $field->getAttribute('name')
                    );
                    $field->setAttribute('name', $nameField);

                    return $field;
                });
            }

            if (HasMany::class === get_class($this->relation)) {
                $this->fields->transform(function (Model $foreignModel) {
                    $foreignFields = (ResourceModel::autoloadFromModel($foreignModel))->getFieldsForEditContext();

                    $nameField = sprintf(
                        'relation_fields[%s][%s]',
                        $this->relation->getRelated()->getTable(),
                        $foreignModel->getKey()
                    );
                    $newField = (new Textarea)
                        ->setAttribute('name', $nameField)
                        ->setAttribute('value', json_encode($foreignModel->toJson()));

                    return $newField;
                });
            }
        }
    }

    public function setRelation(Relation $relation)
    {
        // Belongs to
        if (BelongsTo::class === get_class($relation)) {
            $foreignModel = $relation->first();

            // Set data if exists, or create a new model instance
            if ($foreignModel) {
                $foreignResourceModel = ResourceModel::autoloadFromModel($foreignModel);
            } else {
                $foreignResourceModel = ResourceModel::autoloadFromModel($relation->getRelated());
            }

            $this->relation = $relation;
            $this->fields = $foreignResourceModel->getFieldsForEditContext();
        }

        // Has Many
        if (HasMany::class === get_class($relation)) {
            $foreignRows = $relation->getQuery()
                ->limit(10)
                ->get();

            $this->relation = $relation;
            $this->fields = collect($foreignRows);
        }

        return $this;
    }


}
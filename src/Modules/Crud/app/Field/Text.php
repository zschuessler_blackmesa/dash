<?php

namespace Dash\Modules\Crud\Field;

use Illuminate\Database\Eloquent\Model;
use Dash\Modules\Crud\Field;

class Text extends \Dash\Modules\Crud\Field
{
    public $link;

    public function getValueAttribute()
    {
        if (is_object($this->getRawAttribute('value'))) {
            return serialize($this->getRawAttribute('value'));
        }

        return $this->getRawAttribute('value');
    }
}
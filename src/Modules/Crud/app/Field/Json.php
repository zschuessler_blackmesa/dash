<?php

namespace Dash\Modules\Crud\Field;

use Dash\Modules\Crud\Field;

class Json extends Field
{
    public function getValueAttribute()
    {
        $value = $this->attributes->get('value');

        if (!$value) {
            return null;
        }

        if (is_string($value)) {
            return $value;
        }

        if (is_object($value) || is_array($value)) {
            return json_encode($value, JSON_PRETTY_PRINT);
        }
    }
}
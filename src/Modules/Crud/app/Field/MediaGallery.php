<?php

namespace Dash\Modules\Crud\Field;

use Dash\Modules\Crud\Field;

class MediaGallery extends Field
{
    public function getValueAsArray()
    {
        return json_decode($this->getAttribute('value'));
    }
}
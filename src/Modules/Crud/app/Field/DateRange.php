<?php

namespace Dash\Modules\Crud\Field;

use Dash\Modules\Crud\Field;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcher;

class DateRange extends Field
{
    public $start_date_column = 'start_date';

    public $end_date_column = 'end_date';

    public $value_start;

    public $value_end;

    public function registerEvents(EventDispatcher $dispatcher)
    {
        $dispatcher->addListener('model.set', function(Event $event) {
            $row = $event->model;

            if (!$row) {
                return;
            }

            // Set start date
            $startDateColumn = $this->start_date_column;
            if ($row->$startDateColumn) {
                $this->value_start = $row->$startDateColumn;
            }

            // Set end date
            $endDateColumn = $this->end_date_column;
            if ($row->$endDateColumn) {
                $this->value_end = $row->$endDateColumn;
            }
        });
    }

}
<?php

namespace Dash\Modules\Crud\Field;

use Dash\Modules\Crud\Field;

class Virtual extends Field
{
    public $content = '';

    public $isVirtual = true;
}
<?php

namespace Dash\Modules\Crud\Field;

use Dash\Modules\Crud\Field;
use Dash\Modules\Crud\ResourceModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class SelectOneToOne extends Field
{
    public $options;

    public $foreignModel;

    public $foreignResourceModel;

    public $foreignKey;

    public $image;

    public $ajaxUrl;

    public function __construct()
    {
        parent::__construct();

        $this->options = collect();
    }

    public function getViewValue()
    {
        $foreignModel = $this->foreignModel;
        $defaultNameColumn = config('dash.default_table_name_column');
        $defaultIdColumn = config('dash.default_table_id_column');

        if ($foreignModel && isset($foreignModel->$defaultNameColumn)) {
            return $foreignModel->$defaultNameColumn;
        } else if ($foreignModel && isset($foreignModel->$defaultIdColumn)) {
            return $foreignModel->$defaultIdColumn;
        }

        return '';
    }

    public function getEditValue()
    {
        return $this->getAttribute('value');
    }

    public function getCreateValue()
    {

    }

    public function getTitleThumbnailUrl()
    {
        if ($this->hasTitleThumbnail()) {
            return \Storage::disk('public')
                ->url($this->model->getAttribute(config('dash.default_table_image_column')));
        }
    }

    public function getImageUrl()
    {
        if ($this->hasImage()) {
            return \Storage::disk('public')->get($this->foreignModel->avatar);
        }

        return '';
    }

    public function hasImage()
    {
        /** @var $model Model */
        $foreignModel = $this->foreignModel;

        if (!$foreignModel) {
            return false;
        }

        if (in_array('avatar', array_keys($foreignModel->getAttributes()))) {
            return true;
        }

        return false;
    }

    public function hasForeignEditRoute()
    {
        $routeName = $this->getForeignEditRouteName();

        if($routeName && \Route::has($routeName)) {
            return true;
        }

        return false;
    }

    public function getForeignEditRouteName()
    {
        $foreignModel = $this->foreignModel;

        if (!$foreignModel || !$foreignModel->exists) {
            return;
        }

        $routeName =  sprintf('%s.%s.edit', config('dash.default_route_namespace'), $foreignModel->getTable());

        if($routeName && \Route::has($routeName)) {
            return $routeName;
        }

        return false;
    }

    public function getForeignSearchRouteName()
    {
        return 'dash.crud.search';
    }

    public function getForeignResourceModel()
    {
        if ($this->foreignResourceModel) {
            return $this->foreignResourceModel;
        }

        $model = $this->foreignModel;

        // Match path: App\Dash\ResourceModels\{ModelName}.php
        $className = sprintf('App\Dash\ResourceModels\%s', studly_case(str_singular($model->getTable())));
        if (class_exists($className)) {
            return (new $className($model));
        }

        // Check if there is a default Dash resource model class
        $className = sprintf('Dash\ResourceModel\%s', studly_case(str_singular($model->getTable())));
        if (class_exists($className)) {
            return (new $className($model));
        }

        return (new ResourceModel($model));
    }

    public function foreignResourceModel(ResourceModel $resourceModel)
    {
        $this->foreignResourceModel = $resourceModel;

        return $this;
    }

    public function foreignModel(Model $model)
    {
        $this->foreignModel = $model;

        // Set default options for editing
        $schema        = \DB::getDoctrineSchemaManager();
        $schemaColumns = collect($schema->listTableColumns($model->getTable()));

        if ($schemaColumns->has(config('dash.default_table_name_column'))) {
            $this->foreignModelOptions(
                $model::select()
                    ->orderBy(config('dash.default_table_name_column'))
                    ->limit(10)
            );

            $nameColumn = config('dash.default_table_name_column');
            $this->viewData->put('selectedOptionTitle', $model->$nameColumn);
        }

        // Set ajax url for editing
        if ($foreignSearchRoute = $this->getForeignSearchRouteName()) {
            $this->ajaxUrl = route($foreignSearchRoute);
        }

        return $this;
    }

    public function foreignModelOptions(Builder $builder)
    {
        $rows = $builder->get();

        $this->viewData->put('options', $rows->mapWithKeys(function(Model $row) {
            return [
                $row->getAttribute(config('dash.default_table_id_column')) => $row->getAttribute(config('dash.default_table_name_column'))
            ];
        }));

        return $this;
    }
}

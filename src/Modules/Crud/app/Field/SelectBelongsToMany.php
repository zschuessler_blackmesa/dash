<?php

namespace Dash\Field;

use Dash\Modules\Crud\Field;
use Dash\Modules\Crud\ResourceModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Symfony\Component\EventDispatcher\EventDispatcher;

class SelectBelongsToMany extends Field
{
    public $foreignModel;

    public $foreignResourceModel;

    public $localModelMethod;

    public function registerEvents(EventDispatcher $dispatcher)
    {
        $dispatcher->addListener('model.set', function() {
            $this->onLocalModelSet();
        });
    }

    public function foreignModel(Model $foreignModel)
    {
        // Set foreign model attributes
        $this->localModelMethod = camel_case($foreignModel->getTable());
        $this->foreignModel     = $foreignModel;

        // If local model was already set, update value
        if ($this->model) {
            $this->updateFieldValue();
        }

        $this->viewData->put('ajaxUrl', route('dash.crud.search'));

        return $this;
    }

    public function updateFieldValue()
    {
        $localModelMethod = $this->localModelMethod;

        $this->viewData->put('value', $this->model->$localModelMethod);

        return $this;
    }

    public function onLocalModelSet()
    {
        if ($this->foreignModel) {
            $this->updateFieldValue();
        }

        return $this;
    }

    public function getForeignResourceModel()
    {
        if ($this->foreignResourceModel) {
            return $this->foreignResourceModel;
        }

        $model = $this->foreignModel;

        // Match path: App\Dash\ResourceModels\{ModelName}.php
        $className = sprintf('App\Dash\ResourceModels\%s', studly_case(str_singular($model->getTable())));
        if (class_exists($className)) {
            return (new $className($model));
        }

        return (new ResourceModel($model));
    }

    public function foreignResourceModel(ResourceModel $resourceModel)
    {
        $this->foreignResourceModel = $resourceModel;

        return $this;
    }
}
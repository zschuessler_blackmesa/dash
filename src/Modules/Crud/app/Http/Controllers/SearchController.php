<?php

namespace Dash\Modules\Crud\Http\Controllers;

use App\Http\Controllers\Controller;
use Dash\Field;
use Dash\ResourceModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Dash\Modules\Core\SchemaMapper;

class SearchController extends Controller
{
    public function handle(Request $request)
    {
        $resourceModelClass = urldecode($request->get('resource_model'));
        $modelClass         = urldecode($request->get('model_class'));

        /** @var $resourceModel ResourceModel */
        $resourceModel = new $resourceModelClass(new $modelClass);

        /* Redirect request based on context if set */
        $requestContext = $request->input('context');
        if ($requestContext && 'ajax_relational_search' === $requestContext) {
            return $resourceModel->respondToSelect2AjaxRequest($request);
        }

        $selectColumns = collect($request->get('columns'))
            ->map(function($columnArray) {
                return $columnArray['data'];
            })
            ->toArray();

        $model = $resourceModel->model->select();

        // Filter: global search
        $defaultNameColumn = $resourceModel->config->get('default_table_name_column');

        if ($request->input('search.value') && collect($selectColumns)->contains($defaultNameColumn)) {
            $model->where($defaultNameColumn,
                'like',
                '%' . strtolower($request->input('search.value')) . '%'
            );
        }

        // Paginate
        $model = $model->paginate($request->input('length'), ['*'], 'page', $request->input('start'));

        /**
         * Transform response
         */
        /** @var $schemaColumns Collection */
        $schemaMapper = new SchemaMapper;
        $datagridFields = $resourceModel->datatableFields();

        $response = $datagridFields->mapWithKeys(function(Field $field) {
            return [
                $field->name => $field->renderView('datatable')
            ];
        });

        $response = collect($model->items())
            ->map(function(Model $row) {
                $rowResourceModel = ResourceModel::autoloadFromModel($row);
                $fields = $rowResourceModel->datatableFields();

                return $fields->mapWithKeys(function(Field $rowField) {
                    return [
                        $rowField->name => $rowField->renderView('datatable')
                    ];
                });

                return $rowResourceModel->datatableFields();
            })
            ->toArray();

        $totalTableRecords = (int)$model->count();

        return json_encode([
            'draw'            => (int)$request->input('draw'),
            'recordsTotal'    => $totalTableRecords,
            'recordsFiltered' => $model->total(),
            'data'            => $response,
        ]);
    }
}
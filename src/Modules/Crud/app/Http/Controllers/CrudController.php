<?php

namespace Dash\Modules\Crud\Http\Controllers;

use App\Http\Controllers\Controller;
use Dash\Modules\Crud\ViewModel;
use Dash\Modules\Crud\ViewModel\Layouts\Crud\DefaultLayout as DefaultCrudLayout;
use Illuminate\Http\Request;

use Dash\Modules\Crud\ResourceModel;
use Dash\Modules\Crud\ViewModel\Panel;


class CrudController extends Controller
{
    public $view = 'crud::pages.edit';

    public $routes;

    public $views;

    public $labels;

    public $data = ['title', 'subTitle', 'content', 'model', 'labels', 'routes'];

    public $model;

    public $modelClass;

    public $resourceModelClass;

    public $resourceModel;

    public $context;

    public function __construct(Request $request)
    {
        $this->model = $this->getModel();
        $this->resourceModel = $this->getResourceModel();
        $this->context = $this->getContextFromRoute();
    }

    public function getContextFromRoute()
    {
        /** @var $route \Illuminate\Routing\Route */
        $route = request()->route();

        // If called from CLI, return early
        if (!$route) {
            return;
        }

        $action = $route->getActionName();

        if ($route && $action) {
            if(str_contains($action, '@edit')) {
                return 'edit';
            }
        }
    }

    public function getResourceModel()
    {
        $model = $this->getModel();

        // Return resource model if it's already set
        if ($this->resourceModel) {
            return $this->resourceModel;
        }

        // Default ot the resource model class property if set
        if ($this->resourceModelClass) {
            $this->resourceModel = new $this->resourceModelClass($model);
            return $this->resourceModel;
        }

        // Try to autoload if no other options
        return ResourceModel::autoloadFromModel($model);
    }

    public function index()
    {
        $resourceModel = $this->getResourceModel();

        $datatable = $resourceModel->getDatatable();

        $panel = (new Panel)
            ->content($datatable);

        return view($resourceModel->views->get('index'), [
            'content'   => $panel,
            'model'     => $this->getModel(),
            'datatable' => $datatable,
            'labels'    => $resourceModel->labels,
            'routes'    => $resourceModel->routes,
            'resourceModel' => $resourceModel
        ]);
    }

    public function edit()
    {
        $this->authorize('update', $this->getModel());

        $defaultLayout = $this->getLayoutForEdit();

        return $defaultLayout->createView();
    }

    public function getLayoutForEdit() : ViewModel
    {
        return (new DefaultCrudLayout)
            ->setResourceModel($this->getResourceModel());
    }

    public function create()
    {
        $this->authorize('create', $this->getModel());

        $defaultLayout = $this->getLayoutForCreate();

        return $defaultLayout->createView();
    }

    public function getLayoutForCreate() : ViewModel
    {
        return (new DefaultCrudLayout)
            ->setResourceModel($this->getResourceModel());
    }

    public function update(Request $request)
    {
        $this->saveModel($request);

        $resourceModel = $this->getResourceModel();
        $row = $this->getModel();

        if (!count(\Alert::getMessages())) {
            \Alert::add('toast', $resourceModel->labels->get('singular') . ' updated')->flash();
        }

        return redirect()->route($resourceModel->routes->get('edit'), [$row]);
    }

    public function store(Request $request)
    {
        $this->saveModel($request);
        $newRow = $this->getModel();
        $resourceModel = $this->getResourceModel();

        if (!count(\Alert::getMessages())) {
            \Alert::add('toast', $resourceModel->labels->get('singular') . ' created')->flash();
        }

        return redirect()->route($resourceModel->routes->get('edit'), [$newRow]);
    }

    public function getModel()
    {
        if ($this->model) {
            return $this->model;
        }

        if (!$this->modelClass) {
            throw new \Exception('`modelClass` property is required for CrudController class.');
        }

        // Model not set: create it based on model class specified.
        $route = request()->route();
        $pathModelVariable = $route ? collect($route->getCompiled()->getPathVariables())->first() : null;

        if ($pathModelVariable && $route)  {
            $this->model = $this->modelClass::where(
                    (new $this->modelClass)->getRouteKeyName(),
                    $route->parameter($pathModelVariable)
                )
                ->first();
        } else {
            $this->model = new $this->modelClass;
        }

        return $this->model;
    }

    public function modelClass($className)
    {
        $this->modelClass = $className;
        $this->model = $this->getModel();

        return $this;
    }

    public function saveModel(Request $request)
    {
         $resourceModel = $this->getResourceModel()
            ->saveModel($request);

         $this->model = $resourceModel->getModel();
         $this->resourceModel = $resourceModel;

         return $this;
    }
}
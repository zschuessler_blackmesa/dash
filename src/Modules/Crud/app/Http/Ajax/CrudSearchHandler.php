<?php

namespace Dash\Modules\Crud\Http\Ajax;

use Dash\Modules\Crud\Field;
use Dash\Modules\Core\Services\Ajax\HandlerAbstract;
use Dash\Modules\Crud\ResourceModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class CrudSearchHandler extends HandlerAbstract
{

    public function handle(Request $request)
    {
        $resourceModelClass = urldecode($request->get('resource_model'));
        $modelClass         = urldecode($request->get('model_class'));

        /** @var $resourceModel ResourceModel */
        $resourceModel = new $resourceModelClass(new $modelClass);

        /* Redirect request based on context if set */
        $requestContext = $request->input('context');
        if ($requestContext && 'ajax_relational_search' === $requestContext) {
            return $resourceModel->respondToSelect2AjaxRequest($request);
        }

        $selectColumns = collect($request->get('columns'))
            ->map(function($columnArray) {
                return $columnArray['data'];
            })
            ->toArray();

        $model = $resourceModel->model->select();

        // Filter: global search
        $defaultNameColumn = $resourceModel->config->get('default_table_name_column');

        if ($request->input('search.value') && collect($selectColumns)->contains($defaultNameColumn)) {
            $model->where($defaultNameColumn,
                'like',
                '%' . strtolower($request->input('search.value')) . '%'
            );
        }

        // Paginate
        $model = $model->paginate($request->input('length'), ['*'], 'page', $request->input('start'));

        /**
         * Transform response
         */
        $datagridFields = $resourceModel->datatableFields();

        $editRoutes = collect();
        $editRouteId = $resourceModel->routes->get('edit');
        $response = collect($model->items())
            ->map(function(Model $row) use ($datagridFields, &$editRoutes, $editRouteId) {
                $editRoutes = $editRoutes->push(route(
                    $editRouteId,
                    $row
                ));
                return $datagridFields->mapWithKeys(function(Field $rowField) use ($row) {
                    $newField = clone $rowField;
                    $newField->model($row);
                    $newField->setRawAttributes($rowField->getAttributes()->toArray());
                    $newField->setAttribute('value', $row->getAttribute($newField->getAttribute('name')));

                    return [
                        $newField->getAttribute('name') => $newField->renderView('datatable')
                    ];
                });
            })
            ->toArray();

        $totalTableRecords = (int)$model->count();

        return [
            'draw'            => (int)$request->input('draw'),
            'recordsTotal'    => $totalTableRecords,
            'recordsFiltered' => $model->total(),
            'data'            => $response,
            'editRoutes'      => $editRoutes->toArray()
        ];
    }
}
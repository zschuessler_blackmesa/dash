<?php

namespace Dash\Modules\Crud\Providers;

use Dash\Module\Module;
use Dash\Modules\Crud\Console\MakeCrudCommand;
use Dash\Modules\Core\Services\Ajax;
use Dash\Modules\Crud\Http\Ajax\CrudSearchHandler;
use Dash\Modules\Theme\Providers\AssetServiceProvider;
use Dash\Modules\Theme\Providers\MenuServiceProvider;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Dash\Modules\Theme\Providers\MenuServiceProvider\MenuItem;

class CrudServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        // Register menu items
        $this->registerMenuItems();

        // Register assets in theme
        $this->registerAssets();

        // Register body classes
        $this->registerBodyClasses();
    }

    public function registerBodyClasses()
    {
        \Theme::bodyClasses()->addClass('dash-crud');
    }

    public function registerAssets()
    {
        $assetProvider = app(AssetServiceProvider::class);
        $assetProvider->registerStyle(
            asset('dash/crud/crud.css')
        );
    }

    public function registerMenuItems()
    {
        $menuServiceProvider = app(MenuServiceProvider::class);
        $menuItems = $menuServiceProvider->getMenuItems();
        $appConfig = collect(config()->all());

        // Add any Crud modules to menu
        $crudMenuItems = collect(\Module::all())
            ->filter(function(Module $module) use ($appConfig) {
                $configKey = sprintf('dash-crud-%s', strtolower($module->getSlug()));

                // Dont show disabled modules
                if ($module->disabled()) {
                    return;
                }

                // Only match crud modules
                if (!$appConfig->has($configKey)) {
                    return;
                }

                // Don't show modules that are not set to visible
                $moduleConfigHasVisibility = data_get($appConfig->get($configKey), 'dash-menu.visible');
                if (!$moduleConfigHasVisibility) {
                    return;
                }

                return true;
            })
            ->transform(function(Module $module) use ($appConfig) {
                $routeBasepath = sprintf(
                    '%s.%s',
                    config('dash.default_route_namespace'),
                    Str::slug(Str::kebab(Str::plural($module->getName())))
                );
                $configKey = sprintf('dash-crud-%s', strtolower($module->getSlug()));
                $defaultModuleConfig = [
                    'title'    => $module->getName(),
                    'visible'  => true,
                    'icon'     => 'fas fa-box',
                    'priority' => 100,
                    'route'    => sprintf('%s.index', $routeBasepath)
                ];

                $moduleConfig = array_merge(
                    $defaultModuleConfig,
                    data_get($appConfig->get($configKey), 'dash-menu')
                );

                /**
                 * Add Children
                 */
                $parentItem = new MenuItem($moduleConfig);
                $parentItem->addChild(
                    new MenuItem([
                        'title' => 'View all',
                        'route' => sprintf('%s.index', $routeBasepath),
                        'icon' => 'fas fa-box-full'
                    ])
                )
                    ->addChild(
                        new MenuItem([
                            'title' => 'Create New',
                            'route' => sprintf('%s.create', $routeBasepath),
                            'icon' => 'fas fa-box-full'
                        ])
                    );

                return $parentItem;
            });

        // Register with menu
        $crudMenuItems->each(function(MenuItem $menuItem) use ($menuServiceProvider) {
            $menuServiceProvider->addMenuItem($menuItem);
        });

        return $this;
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);

        // Register config
        $this->registerConfig();

        // Register views
        $this->registerViews();

        // Ajax provider
        app(Ajax::class)->registerHandler(new CrudSearchHandler);

        // Register commands
        $this->commands([
            MakeCrudCommand::class
        ]);
    }

    public function getResourcePath($resourcePath)
    {
        return __DIR__ . '/../../resources/' . $resourcePath;
    }

    public function registerViews()
    {
        $this->publishes(
            [$this->getResourcePath('views') => resource_path('views/dash/crud')],
            'views'
        );

        $viewPaths = [
            $this->getResourcePath('views'),
            resource_path('views/dash')
        ];

        $this->loadViewsFrom($viewPaths, 'crud');
    }

    protected function registerConfig()
    {
        $this->publishes([
            __DIR__ . '/../../config/dash-crud.php' => config_path('dash-crud.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/dash-crud.php', 'dash-crud'
        );
    }
}

<?php

namespace Dash\Modules\Crud\Console;

use Dash\Modules\Crud\Generators\CrudModuleGenerator;
use Illuminate\Console\Command;
use Illuminate\Support\Composer;
use Illuminate\Support\Str;

class MakeCrudCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'crud:make {--overwrite}';

    /**
     * @var string
     */
    protected $description = 'Make a new CRUD module';

    public $composer;

    public function __construct(Composer $composer)
    {
        parent::__construct();

        $this->composer = $composer;
    }

    public function handle()
    {
        // Get module name from asking the table name
        $table = $this->askTableName();
        $moduleName = Str::singular(Str::studly($table));

        // Generate
        $moduleGenerator = (new CrudModuleGenerator([
                'name' => $moduleName
            ]))
            ->overwrite($this->option('overwrite'))
            ->generate();

        $this->composer->dumpAutoloads();

    }

    public function askTableName()
    {
        $tableOptions = \DB::connection()->getDoctrineSchemaManager()->listTableNames();
        $table = $this->anticipate('Table name', $tableOptions);

        // Verify the table exists
        if (!in_array($table, $tableOptions)) {
            $this->error('Invalid table name: table does not exist');

            return $this->askTableName();
        }

        // Verify table wasn't already generated
        $crudPath = base_path(
            sprintf('app/Modules/Crud/%s/', Str::singular(Str::studly($table)))
        );
        if (is_dir($crudPath) && !$this->option('overwrite')) {
            $this->error('Module already exists. Use --overwrite to create the module anyway.');
            $this->error($crudPath);
            exit;
        }

        return $table;
    }
}

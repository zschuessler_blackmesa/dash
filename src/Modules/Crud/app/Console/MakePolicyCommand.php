<?php

namespace Dash\Modules\Crud\Console;

use Dash\Module\Commands\GeneratorCommand;
use Dash\Module\Support\Config\GenerateConfigReader;
use Dash\Module\Support\Stub;
use Dash\Module\Traits\ModuleCommandTrait;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputArgument;

class MakePolicyCommand extends GeneratorCommand
{
    use ModuleCommandTrait;

    protected $argumentName = 'name';

    /**
     * The command name.
     *
     * @var string
     */
    protected $name = 'crud:make-policy';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'Create a new route service provider for the specified module.';

    /**
     * The command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['module', InputArgument::OPTIONAL, 'The name of module will be used.'],
            ['name', InputArgument::OPTIONAL, 'The name of module will be used.'],
            ['configPrefix', InputArgument::OPTIONAL, 'The config file to use'],
        ];
    }

    /**
     * Get template contents.
     *
     * @return string
     */
    protected function getTemplateContents()
    {
        $configPrefix = $this->getConfigPrefix();

        $module = $this->laravel[$configPrefix]->findOrFail($this->getModuleName());

        return (new Stub('/policy.stub', [
            'NAMESPACE'     => $this->getClassNamespace($module),
            'CLASS'         => $this->getFileName(),
            'MODEL_INCLUDE' => $this->getModelInclude(),
            'MODEL_NAME'    => $this->getModuleName(),
        ]))->render();
    }

    public function getModelName()
    {
        return Str::studly($this->getModuleName());
    }

    public function getModelInclude()
    {
        $path = config('dash.default_model_path');

        // Convert all folder names to uppercase; swap slash type
        $path = collect(explode('/', $path))
            ->transform(function($folderPath) {
                return ucwords($folderPath);
            })
            ->implode('\\');

        // Add Slash to end if it doenst exist
        $path =  rtrim($path, '\\') . '\\';

        // Add model name
        $path .= $this->getModelName();

        return $path;
    }

    /**
     * @return string
     */
    private function getFileName()
    {
        return $this->getModelName() . 'Policy';
    }

    /**
     * Get the destination file path.
     *
     * @return string
     */
    protected function getDestinationFilePath()
    {
        $configPrefix = $this->getConfigPrefix();
        $path = $this->laravel[$configPrefix]->getModulePath($this->getModuleName());

        $generatorPath = GenerateConfigReader::read('policy');

        return $path . $generatorPath->getPath() . '/' . $this->getFileName() . '.php';
    }

    public function getDefaultNamespace() : string
    {
        $configPrefix = $this->getConfigPrefix();
        return $this->laravel[$configPrefix]->config('paths.generator.policy.path', 'Policies');
    }
}

<?php

namespace $CLASS_NAMESPACE;

use Dash\Http\Controllers\CrudController;

use $MODEL_INCLUDE;

class $CLASS_NAME extends CrudController
{
    public $modelClass = $MODEL_NAME::class;
}
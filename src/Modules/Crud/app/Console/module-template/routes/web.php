<?php

/*
|--------------------------------------------------------------------------
| $MODULE_NAME Crud Resource Routes
|--------------------------------------------------------------------------
|
| Default resource routes generated for $MODULE_NAME
|
*/

Route::group([
    'middleware' => $DASH_MIDDLEWARE,
        'as'         => '$DASH_ROUTE_NAMESPACE.',
        'prefix'     => '$DASH_ROUTE_NAMESPACE'
    ],
    function () {
        // $MODULE_NAME Resource Route
        Route::resource('$MODULE_SLUG', '$MODULE_NAMEController');
    }
);

const { mix } = require('laravel-mix');

mix.setPublicPath('../../public').mergeManifest();

mix.js(__dirname + '/Resources/assets/js/app.js', 'js/congressionaldistrict.js')
    .sass( __dirname + '/Resources/assets/sass/app.scss', 'css/congressionaldistrict.css');

if (mix.inProduction()) {
    mix.version();
}
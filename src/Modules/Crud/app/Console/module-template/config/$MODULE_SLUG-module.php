<?php

return [
    'name' => '$MODULE_NAME',
    'dash-menu' => [
        'title'    => '$MODULE_TITLE',
        'visible'  => true,
        'icon'     => 'fas fa-box',
        'priority' => 100
    ],
    'dash-media-gallery' => [
        'enabled' => false,
        'folder'  => '$MODULE_SLUG',
        'driver'  => 'local'
    ]
];

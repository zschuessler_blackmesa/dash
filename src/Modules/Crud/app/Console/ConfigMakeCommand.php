<?php

namespace Dash\Modules\Crud\Console;

use Dash\Module\Commands\GeneratorCommand;
use Illuminate\Support\Str;
use Dash\Module\Module;
use Dash\Module\Support\Config\GenerateConfigReader;
use Dash\Module\Support\Stub;
use Dash\Module\Traits\ModuleCommandTrait;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class ConfigMakeCommand extends GeneratorCommand
{
    use ModuleCommandTrait;

    /**
     * The name of argument name.
     *
     * @var string
     */
    protected $argumentName = 'name';

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'crud:make-config';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new crud config';

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The service provider name.'],
            ['module', InputArgument::OPTIONAL, 'The name of module will be used.'],
            ['configPrefix', InputArgument::OPTIONAL, 'The configuration file to use'],
        ];
    }


    /**
     * @return mixed
     */
    protected function getTemplateContents()
    {
        $configPrefix = $this->getConfigPrefix();

        /** @var Module $module */
        $module = $this->laravel[$configPrefix]->findOrFail($this->getModuleName());

        Stub::setBasePath($this->getStubPath());

        return (new Stub('/config.stub', [
            'STUDLY_NAME'        => $module->getStudlyName(),
            'NAME_LOWERCASE'     => strtolower($module->getStudlyName()),
            'STUDLY_NAME_PLURAL' => str_plural($module->getStudlyName()),
        ]))->render();
    }

    /**
     * @return mixed
     */
    protected function getDestinationFilePath()
    {
        $configPrefix = $this->getConfigPrefix();

        $path = $this->laravel[$configPrefix]->getModulePath($this->getModuleName());

        $generatorPath = GenerateConfigReader::read('config');

        $finalPath = $path . $generatorPath->getPath() . '/' . $this->getFileName() . '.php';

        return $finalPath;
    }

    /**
     * @return string
     */
    private function getFileName()
    {
        $fileName = sprintf('dash-crud-%s', strtolower($this->argument('name')));

        return $fileName;
    }
}

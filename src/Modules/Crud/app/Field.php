<?php

namespace Dash\Modules\Crud;

use Dash\Modules\Core\Traits\HasAttributes;
use Dash\Modules\Core\Traits\HasMagicMethods;
use Dash\Modules\Core\Traits\HasMagicProperties;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Schema\ForeignKeyConstraint;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Field
{
    use HasAttributes;
    use HasMagicMethods;
    use HasMagicProperties;

    /**
     * Doctrine
     *
     * The doctrine object returned from SchemaMapper.
     *
     * @var AbstractPlatform
     */
    protected $doctrine;

    /**
     * View Data
     *
     * A collection of additional view data to pass into template views.
     *
     * @var \Illuminate\Support\Collection
     */
    protected $viewData;

    /**
     * Slug
     *
     * A unique identifier for the field. Useful for autoloading templates and configurations.
     *
     * @var string
     */
    protected $slug;

    /**
     * Views
     *
     * A collection of views. Each key is often a context (eg edit, view, index).
     *
     * @var \Illuminate\Support\Collection
     */
    protected $views;

    /**
     * Model
     *
     * The model attached to this field. Only required if the field is not a virtual field.
     *
     * @var Model
     */
    protected $model;

    /**
     * Context
     *
     * An identifier for the current context the field is being called under.
     * Useful for configuring a field based on common contexts like create, edit, ajax, datatable, etc.
     *
     * @var string
     */
    protected $context;

    /**
     * Label
     *
     * The field label to render in the form output.
     *
     * @var string
     */
    protected $label;

    /**
     * Required
     *
     * True when the field was marked as required in the schema.
     *
     * @var bool
     */
    protected $required;

    /**
     * Default
     *
     * The default value for the field specified in the schema.
     *
     * @var string
     */
    protected $default;

    /**
     * Foreign Key
     *
     * The foreign key field as loaded by Doctrine.
     *
     * @var $foreignKey null|ForeignKeyConstraint
     */
    protected $foreignKey;

    /**
     * Is Virtual
     *
     * True when the field is flagged as not existing in the schema.
     *
     * @var bool
     */
    protected $isVirtual;

    /**
     * Render Method
     *
     * An optional method to set for custom rendering of the field.
     * Useful when programatically extending a field for a special use case, such as ajax operations.
     *
     * @var function
     */
    protected $renderMethod;

    public function __construct()
    {
        $this->slug = $this->slug ?? Str::slug(Str::kebab(class_basename($this)));

        $this->viewData = collect($this->viewData)->mapWithKeys(function ($key, $value) {
            return [$key => null];
        });

        $this->attributes = collect([
                'value' => null,
                'class' => null
            ])
            ->merge(collect($this->attributes));

        $this->views = collect([
            'view' => sprintf('crud::field.%s.view', $this->slug),
            'edit' => sprintf('crud::field.%s.edit', $this->slug),
            'create' => sprintf('crud::field.%s.create', $this->slug),
            'datatable' => sprintf('crud::field.%s.datatable', $this->slug),
        ]);
    }

    public function setRenderMethod($renderMethod)
    {
        if (is_callable($renderMethod)) {
            $this->renderMethod = $renderMethod;
        }

        return $this;
    }

    public function getRenderMethod()
    {
        if (!$this->renderMethod) {
            $this->renderMethod = function(Field $field, $view) {
                $view = view($field->views()->get($view), ['field' => $field]);
                return $view->render();
            };
        }

        return $this->renderMethod;
    }

    public function renderView($viewKey)
    {
        return call_user_func($this->getRenderMethod(), $this, $viewKey);
    }

    public function getClassAttribute()
    {
        $classAttribute = $this->getRawAttribute('class');

        // If class attribute was explicitly set, return that instead of defaults.
        if ($classAttribute) {
            return $classAttribute;
        }

        // Create default class attribute
        $classes = collect('form-control');

        if ($this->context()) {
            // Set field class: field-{fieldname}
            $fieldClass = sprintf('field-%s', $this->getAttribute('name'));

            // Set context class: field-{fieldname}-{contextname}
            // Relation fields have invalid class name characters, let's deal with them too
            $contextClass = sprintf('field-%s-%s', $this->getAttribute('name'), $this->context());
            // Relation fields have invalid class name characters, let's deal with them
            $contextClass = str_replace('[', '-', $contextClass);
            $contextClass = str_replace(']', '', $contextClass);
            $classString = sprintf('%s %s', $fieldClass, $contextClass);

            $classes->push($classString);
        }

        $this->setRawAttribute('class', trim($classes->implode(' ')));

        return $this->getAttribute('class');
    }

    public function hydrate(AbstractPlatform $dbalPlatform, Column $dbalColumn)
    {
        if (!$this->model()) {
            throw new \Exception('Attempted to hydrate a field without first setting the model.');
        }

        // Properties
        $this->doctrine((object)[
            'platform' => $dbalPlatform,
            'column' => $dbalColumn,
        ]);
        $this->setAttribute('name', $dbalColumn->getName());
        $this->label(str_replace(' Id', '', ucwords(implode(' ', explode('_', $dbalColumn->getName())))));
        $this->required($dbalColumn->getNotNull());
        $this->default($dbalColumn->getDefault());
        $this->setAttribute('value', $this->model->getAttribute($dbalColumn->getName()));
        $this->setAttribute('class', $this->getClassAttribute());

        // Maxlength
        $maxlength = $dbalColumn->getType()->getDefaultLength($dbalPlatform);
        if ($maxlength) {
            $this->setAttribute('maxlength', $maxlength);
        }

        return $this;
    }

    public function morphType($className)
    {
        if (!class_exists($className)) {
            throw new \Exception(sprintf('Attempted to morph to unknown field type `%s`', $className));
        }

        $field = (new $className($this))
            ->model($this->model())
            ->hydrate($this->doctrine()->platform, $this->doctrine()->column);

        return $field;
    }
}

<?php

namespace Dash\Modules\Crud;

use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Schema\ForeignKeyConstraint;
use Illuminate\Database\Eloquent\Model;
use Dash\Modules\Crud\Field\Field;
use Dash\Modules\Crud\Field\FeaturedImage;
use Dash\Modules\Crud\Field\SelectOneToOne;
use Dash\Modules\Crud\Field\Text;

class SchemaMapper
{
    public function __construct()
    {
        $this->schema = \DB::getDoctrineSchemaManager();
        $this->registerColumnTypes();
    }

    public function registerColumnTypes()
    {
        $this->schema->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }

    /**
     * Autoload Schema Field Classes
     *
     * Returns default field classes for all columns/fields in a given model.
     *
     * If the row contains data, the fields will be instantiated with the data.
     *
     * @param Model $model
     * @return \Illuminate\Support\Collection
     * @throws \Doctrine\DBAL\DBALException
     */
    public function autoloadSchemaFieldClasses(Model $model)
    {
        $schema        = \DB::getDoctrineSchemaManager();
        $schemaColumns = collect($schema->listTableColumns($model->getTable()));
        $doctrineConnection = $model->getConnection()->getDoctrineConnection();
        $doctrinePlatform   = $doctrineConnection->getDatabasePlatform();
        $fields = collect();

        /**
         * Create new instance of the field for each schema column
         */
        foreach($schemaColumns as $columnName => $schemaColumn) {
            $field = $this->autoloadSchemaFieldClass($model, $columnName)
                ->model($model)
                ->hydrate($doctrinePlatform, $schemaColumn);
            
            $fields->push($field);
        }

        return $fields;
    }

    public function autoloadSchemaFieldClass(Model $model, $columnName)
    {
        /**
         * Load Model Configurations
         *
         * Rely on Doctrine to get default data for the model and columns.
         */
        $schema        = \DB::getDoctrineSchemaManager();
        $schemaColumns = collect($schema->listTableColumns($model->getTable()));
        $doctrineConnection = $model->getConnection()->getDoctrineConnection();
        $doctrinePlatform   = $doctrineConnection->getDatabasePlatform();
        $fields = collect();
        $schemaColumn  = $schemaColumns->get($columnName);
        $columnType    = $schemaColumn->getType()->getName();

        // Foreign Keys
        $schemaForeignKeys = collect($schema->listTableForeignKeys($model->getTable()));
        $foreignKeyNames = $schemaForeignKeys->map(function($foreignKey) {
                /** @var $foreignKey ForeignKeyConstraint */
                return $foreignKey->getLocalColumns();
            })
            ->flatten();

        /**
         * 1 - Foreign Keys
         *
         * If the column is a foreign key, either load the class from the external table or use its default.
         *
         * As an example: a foreign key of `user_id` Will attempt to look for this field:
         * App\Dash\Fields\User\{NameField}
         */
        // Currently only supports One to One relationships
        if ($foreignKeyNames->contains($columnName)) {
            /** @var $foreignKey ForeignKeyConstraint */
            $foreignKey = $schemaForeignKeys->filter(function($schemaForeignKey) use ($columnName) {
                    return collect($schemaForeignKey->getLocalColumns())->contains($columnName);
                })
                ->first();

            /**
             * Foreign key relationships
             */
            if (in_array(config('dash.default_table_id_column'), $foreignKey->getForeignColumns())) {
                $foreignTable  = $foreignKey->getForeignTableName();
                $foreignMethod = str_singular($foreignTable);
                $foreignNameField = config('dash.default_table_name_column');
                $foreignIdField   = config('dash.default_table_id_column');

                /**
                 * 1 - Check if foreign table has a Dash Field class for the default name column
                 */
                $foreignField = (new SelectOneToOne());

                // Set foreign key property on field
                $foreignField->foreignKey = $foreignKey;

                /**
                 * 2 - Check if Model Relationship Exists
                 */
                // Normal foreign method naming conventions: $model->$foreignMethod
                if (isset($model->$foreignMethod)) {
                    return $foreignField->foreignModel($model->$foreignMethod);
                } elseif(\method_exists($model, 'parent')) {
                    // Special case for self-referencing tables: $model->parent
                    $parentRow = $model->parent ?? new $model;
                    return $foreignField->foreignModel($parentRow);
                } else {
                    // See if we can guess the model class based on the table name
                    $foreignTableClass = studly_case(str_singular($foreignKey->getForeignTableName()));
                    $guessPaths = ['App\\', 'App\\Models\\'];

                    foreach ($guessPaths as $classPath) {
                        if (class_exists($classPath . $foreignTableClass)) {
                            $foreignModelClassName = $classPath . $foreignTableClass;

                            $localKeyName = collect($foreignKey->getLocalColumns())->first();
                            $foreignModel = (new $foreignModelClassName)
                                ->where($foreignIdField, $model->$localKeyName)
                                ->first();

                            if (!$foreignModel) {
                                $foreignModel = (new $foreignModelClassName);
                            }
                            return $foreignField->foreignModel($foreignModel);
                        }
                    }

                    // If not, user needs to tell us where it is
                    throw new \Exception(sprintf(
                        'SchemaMapper found a foreign key, but no configuration for table `%s` on key `%s` referencing `%s`',
                        $model->getTable(),
                        $foreignKey->getName(),
                        $foreignKey->getForeignTableName()
                    ));
                }
            }
        }

        /**
         * 2 - Match Default Classes
         *
         * Some column types are mapped to special Dash classes. As an example, you can have a `slug`
         * or `image` field on a database simply by naming the columns as such.
         *
         * If no configuration was found at this point, and no special class exists, load a default Field for
         * the given schema column type. As an example, a string column will be a Text Field. An int column will be a
         * Number field.
         */
        // First check for default Dash classes
        $customDashDefaultTypes = collect([
            'image' => FeaturedImage::class
        ]);
        if ($customDashDefaultTypes->has($columnName)) {
            $customDashClassName = $customDashDefaultTypes->get($columnName);
            return new $customDashClassName;
        }

        // No matches so far, autoload the default field from the column type
        switch ($columnType) {
            case 'enum':
                $schemaColumnType = 'text';
                break;
            case 'integer':
                $schemaColumnType = 'text';
                break;
            case 'string':
                $schemaColumnType = 'text';
                break;
            case 'text':
                $schemaColumnType = 'textarea';
                break;
            case 'datetime':
                $schemaColumnType = 'datetime';
                break;
            case 'image':
                $schemaColumnType = 'FeaturedImage';
                break;
            case 'json':
                $schemaColumnType = 'json';
                break;
            default:
                $schemaColumnType = 'text';
                break;
        }

        return ($this->getDefaultFormFieldClass($schemaColumnType))->model($model);
    }

    public function getDefaultFormFieldClass($htmlFieldType)
    {
        $className = ucwords($htmlFieldType);
        $fqName    = 'Dash\Modules\Crud\Field\\' . $className;

        if ($className && class_exists($fqName)) {
            return (new $fqName);
        }

        return new Text;
    }
}
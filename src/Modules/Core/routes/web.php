<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Default index page
Route::get(config('dash.default_route_namespace'), '\Dash\Modules\Core\Http\Controllers\CoreController@index')
    ->middleware(config('dash.default_middleware'))
    ->name(config('dash.default_route_namespace'));

// Ajax service
Route::post('/ajax-service', '\Dash\Modules\Core\Http\Controllers\CoreController@ajax')
    ->middleware(config('dash.default_middleware'))
    ->name('dash.ajax-service');

// Additional default Dash routes
Route::group([
    'prefix'     => config('dash.default_route_namespace'),
    'middleware' => config('dash.default_middleware'),
    'as'         => config('dash.default_route_namespace') . '.',
    'namespace'  => strtoupper(config('dash.default_route_namespace'))
], function() {
    //
});

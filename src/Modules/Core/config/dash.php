<?php

return [
    /**
     * Default Table ID Column
     *
     * This is the column that is assumed to be the unique identifier for the table.
     * This is shown by default in datagrids and to identify a unique row in a dataset.
     */
    'default_table_id_column' => 'id',

    /**
     * Default Table Name Column
     *
     * This is the column that is assumed to be used for a title or name field.
     * This default is used when displaying data in datagrids, as an example.
     *
     * Use whatever default you like best; you might use `title` for example.
     */
    'default_table_name_column' => 'name',

    /**
     * Default Image Column
     *
     * This is the column that is assumed to be the main image field for a table.
     * This is shown by default in datagrids and relational fields.
     */
    'default_table_image_column' => 'image',

    /**
     * Default Table Sort Column
     *
     * The column assumed to sort on, if it exists. If it doesn't exist, no sort will take place without
     * further configuration by the user.
     */
    'default_table_sort_column' => 'created_at',

    /**
     * Default Table Row Limit
     *
     * The number of records to return by default. Tables work with AJAX; it's recommended for performance reasons
     * to keep this around 10-20 for large tables.
     */
    'default_table_row_limit' => 10,

    /**
     * Default Route Prefix
     *
     * This is the default name used to namespace the path to this dashboard.
     * When set, routes and paths will be prefixed with the name. Default value example:
     *
     * Default URL: /admin
     * Default route for an edit page: admin.user.edit
     */
    'default_route_namespace' => 'admin',

    /**
     * Default Autoload Path
     *
     * Sets the directory where custom Davinci entities may be autoloaded.
     * As an example, you might want to create a custom field type and have them autoloaded.
     */
    'default_autoload_path' => app_path('Dash'),

    /**
     * Default Model Path
     *
     * Sets the directory where App models live.
     * While Laravel defaults to `app/`, many people make a folder for it like `app/Models`
     */
    'default_model_path' => 'app',

    /**
     * Default Middleware
     *
     * The default middleware to apply to the Dash instance.
     */
    'default_middleware' => ['verified', 'auth'],

    'modules' => [
        'namespace' => 'App\Modules',
        'path'      => base_path('app/Modules'),
    ]
];

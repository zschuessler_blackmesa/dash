<?php

namespace Dash\Modules\Core\Providers;

use Dash\Modules\Core\Console\MakeCrudCommand;
use Dash\Modules\Core\Services\Ajax;
use Dash\Modules\Core\Services\Ajax\CrudSearchHandler;
use Dash\Modules\MediaLibrary\Providers\MediaLibraryServiceProvider;
use Illuminate\Support\ServiceProvider;

class CoreServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);

        // Ajax provider
        $this->app->instance(Ajax::class, new Ajax);

        // MediaLibrary provider
        $this->app->register(MediaLibraryServiceProvider::class);
    }
}

<?php

namespace Dash\Modules\Core\Services\Ajax;

use Illuminate\Http\Request;

abstract class HandlerAbstract
{
    public function handle(Request $request)
    {
        return response()->json();
    }
}
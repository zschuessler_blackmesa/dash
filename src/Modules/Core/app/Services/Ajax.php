<?php

namespace Dash\Modules\Core\Services;

use Dash\Modules\Core\Services\Ajax\HandlerAbstract;
use Illuminate\Http\Request;

class Ajax
{
    public $name;

    public $response;

    public $handlers;

    public function __construct()
    {
        $this->handlers = collect();
    }

    public function getHandler($name)
    {
        return $this->handlers->get($name);
    }

    public function registerHandler(HandlerAbstract $handler)
    {
        if (!$this->handlers->contains($handler)) {
            $this->handlers->put(
                get_class($handler),
                $handler
            );
        }

        return $this;
    }

    public function respond($response)
    {
        return response()->json(
            $response
        );
    }



    public function handleRequest(Request $request)
    {
        $ajaxHandler = urldecode($request->input('handler'));

        if (!$this->handlers->get($ajaxHandler)) {
            return $this->respond([
                'success' => false,
                'message' => sprintf('Ajax handler not found: `%s`', $ajaxHandler ?? 'null')
            ]);
        }

        return $this->respond(
            $this->handlers->get($ajaxHandler)->handle($request)
        );
    }
}
<?php

namespace Dash\Modules\Core\Traits;

use Illuminate\Support\Str;

trait HasMagicProperties
{
    public function __get($property)
    {
        $methodName = sprintf('get%s', Str::camel($property));

        if (method_exists($this, $methodName)) {
            return call_user_func([$this, $methodName]);
        }

        throw new \Exception(sprintf(
            'Attempted to get unknown property: %s::%s',
            __CLASS__,
            $property
        ));
    }

    public function __set($property, $value)
    {
        $methodName = sprintf('set%s', Str::camel($property));

        if (method_exists($this, $methodName)) {
            return call_user_func([$this, $methodName]);
        }

        throw new \Exception(sprintf(
            'Attempted to set unknown property: %s::%s',
            __CLASS__,
            $property
        ));
    }
}

<?php

namespace Dash\Modules\Core\Traits;

use Illuminate\Support\Str;

trait HasMagicMethods
{
    public function __call($calledKey, $arguments)
    {
        /**
         * Dynamic methods that do not include getter & setters
         *
         * Example match:
         * $instance->myPropertyName()
         *
         * In the above example, return the `getMyPropertyName` method.
         */
        if (!Str::startsWith($calledKey, ['get', 'set'])) {
            $methodName = sprintf('get%s', ucfirst($calledKey));

            // Call method if it exists
            if (method_exists($this, $methodName)) {
                $value = call_user_func([$this, $methodName]);

                // If a function is returned, call it
                if ($value instanceof \Closure) {
                    return call_user_func_array($value, $arguments);
                }
            }

            // Lookup property
            if (property_exists($this, $calledKey)) {
                if (count($arguments)) {
                    $this->$calledKey = $arguments[0];

                    return $this;
                } else {
                    return $this->$calledKey;
                }
            }
        }

        /**
         * Get & Set Methods
         *
         * Allows support for calling `getMyPropertyName` and `setMyPropertyName` magically.
         */
        // Set method: called when an argument is given
        if (count($arguments)) {
            $value      = $arguments[0];
            $methodName = sprintf('set%s', ucfirst($calledKey));

            if (method_exists($this, $methodName)) {
                // Bind value to the current instance if it's a function
                if (is_callable($value)) {
                    $value->bindTo($this, $this);
                }

                return call_user_func([$this, $methodName], $value);
            }
        }
        // Get method: called when no arguments are given
        if (!count($arguments)) {
            $methodName = sprintf('get%s', ucfirst($calledKey));

            if (method_exists($this, $methodName)) {
                return call_user_func([$this, $methodName]);
            }
        }

        // All lookups failed; return exception
        throw new \Exception(sprintf(
            'Unable to find method for class: %s::%s',
            __CLASS__,
            $calledKey
        ));
    }
}

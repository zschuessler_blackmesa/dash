<?php

namespace Dash\Modules\Core\Http\Controllers;

use Dash\Modules\Core\Services\Ajax;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class CoreController extends Controller
{
    public function index()
    {
        return view('theme::index');
    }

    public function ajax(Request $request, Ajax $ajaxService)
    {
        return $ajaxService->handleRequest($request);
    }
}

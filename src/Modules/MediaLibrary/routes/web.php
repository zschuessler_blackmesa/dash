<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


$defaultNamespace = config('dash.default_route_namespace');

Route::group([
    'prefix' => sprintf('%s/media-library', $defaultNamespace),
    'namespace' => '\Dash\Modules\MediaLibrary\Http\Controllers',
    'as' => 'dash.media-library.'
], function () {
    // Index page
    Route::get('index', 'MediaLibraryController@index')
        ->name('index');
});




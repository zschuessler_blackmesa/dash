<?php

return [
    'name' => 'Media Library',
    'dash-menu' => [
        'title'    => 'Media Library',
        'visible'  => true,
        'icon'     => 'fa fa-picture-o',
        'priority' => 100,
        'route'    => 'dash.media-library.index'
    ],
    'file-system' => [
        'disk' => 'public'
    ]
];

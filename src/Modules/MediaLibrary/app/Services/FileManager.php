<?php

namespace Dash\Modules\MediaLibrary\Services;

use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class FileManager
{
    public $disk;

    public function __construct()
    {
        $this->disk = config('dash-media-library.file-system.disk');
    }

    public function directories($path)
    {
        /** @var $disk FilesystemAdapter */
        $disk = Storage::disk($this->disk);
        $rootPath = $disk->getDriver()->getAdapter()->getPathPrefix();

        $directories = collect($disk->directories($path))
            ->transform(function($directoryName) use ($disk, $rootPath) {
                $path = $disk->path($directoryName);
                $relativePath = str_replace($rootPath, '', $path);

                return (object)[
                    'path'  => $relativePath,
                    'icon'  => 'fa-folder',
                    'label' => Str::title(
                        collect(explode(DIRECTORY_SEPARATOR, $path))->last()
                    )
                ];
            });

        return $directories;
    }

    public function files($path)
    {
        /** @var $disk FilesystemAdapter */
        $disk = Storage::disk($this->disk);

        $files = collect($disk->files($path))
            ->transform(function ($filepath) use ($disk, $path) {
                $fileName = collect(explode('/', $filepath))
                    ->last();

                if (Str::contains($fileName, ['png', 'jpg', 'jpeg', 'gif'])) {
                    $fileType = 'image';
                } else {
                    $fileType = 'file';
                }

                return (object)[
                    'url'   => $disk->url($filepath),
                    'path'  => $filepath,
                    'label' => $fileName,
                    'icon'  => 'fa-file',
                    'type'  => $fileType
                ];
            });

        return $files;
    }


    public function list($path)
    {
        $directories = $this->directories($path);
        $files = $this->files($path);

        return collect([
                'directories' => $directories,
                'files'       => $files
            ]);
    }

    public function uploadFile(Request $request)
    {
        /** @var $file UploadedFile */
        $file = collect($request->files)->first();

        /** @var $disk FilesystemAdapter */
        $disk = Storage::disk($this->disk);

        // Don't allow relative paths, for security reasons
        $path = str_replace('.', '', $request->input('path'));

        $uploadedFile = $disk->putFileAs(
            $path,
            $file,
            $file->getClientOriginalName()
        );
    }
}
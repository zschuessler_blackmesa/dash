<?php

namespace Dash\Modules\MediaLibrary\Http\Ajax;

use Dash\Modules\Crud\Field;
use Dash\Modules\Core\Services\Ajax\HandlerAbstract;
use Dash\Modules\MediaLibrary\Services\FileManager;
use Dash\Modules\Crud\ResourceModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class FileManagerHandler extends HandlerAbstract
{

    public function handle(Request $request)
    {
        $path = str_replace('.', '', $request->input('path'));
        $fileManager = app(FileManager::class);

        // Read-only actions require no further logic
        if (!$request->input('action')) {
            $responseCollection = $fileManager->list($path);

            return $this->sendJsonResponse($responseCollection);
        }

        // Handle upload
        if ('upload' === $request->input('action')) {
            $fileManager->uploadFile($request);
            return $this->sendJsonResponse($fileManager->list($path));
        }
    }

    public function sendJsonResponse(Collection $responseCollection)
    {
        $request = request();

        $responseCollection->put('currentDirectory', (object)[
            'path'  => '/',
            'label' => 'Home',
            'icon'  => 'fa-home'
        ]);

        return $responseCollection->toJson();
    }

}
<?php

namespace Dash\Modules\MediaLibrary\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class MediaLibraryController extends Controller
{
    public function index(Request $request)
    {
        return view('media-library::index');
    }
}

<?php

namespace Dash\Modules\MediaLibrary\Providers;

use Dash\Modules\Core\Services\Ajax;
use Dash\Modules\MediaLibrary\Http\Ajax\FileManagerHandler;
use Dash\Modules\Theme\Providers\AssetServiceProvider;
use Illuminate\Support\ServiceProvider;

class MediaLibraryServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {


    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);

        $this->registerViews();
        $this->registerConfig();
        $this->registerPublicAssets();
        $this->registerAjaxConfig();
    }

    public function registerAjaxConfig()
    {
        // Ajax provider
        app(Ajax::class)->registerHandler(new FileManagerHandler);
    }

    public function registerPublicAssets()
    {
        // Publish /public/ assets
        $this->publishes([
            __DIR__ . '/../../public/' => public_path('dash/media-library'),
        ], 'public');
    }

    protected function registerConfig()
    {
        $this->publishes([
            __DIR__ . '/../../config/dash-media-library.php' => config_path('dash-media-library.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/dash-media-library.php', 'dash-media-library'
        );
    }

    public function getResourcePath($resourcePath)
    {
        return __DIR__ . '/../../resources/' . $resourcePath;
    }

    public function registerViews()
    {
        $this->publishes(
            [$this->getResourcePath('views') => resource_path('views/dash/media-library')],
            'views'
        );

        $viewPaths = [
            $this->getResourcePath('views'),
            resource_path('views/dash')
        ];

        $this->loadViewsFrom($viewPaths, 'media-library');
    }
}

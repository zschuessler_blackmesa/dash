<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dash_media', function (Blueprint $table) {
            $table->increments('id');

            // Content
            $table->string('name');
            $table->string('mime_type');
            $table->string('size');
            $table->string('storage_path');

            // Meta Table
            $table->jsonb('meta')->nullable();

            // Creator
            $table->integer('created_by_user_id')->unsigned()->index();
            $table->foreign('created_by_user_id')->references('id')->on('users')->onDelete('cascade');

            // Meta
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

@extends('theme::layouts.default')

@push('before_head_end')
    <link rel="stylesheet" href="{{ asset('dash/media-library/media-library.css') }}">
@endpush

@section('content')
    <h1>Media Library</h1>

    <div id="DashMediaLibraryComponent">
        <dash-media-library
        ajax-endpoint-prop="{{ route('dash.ajax-service') }}"
        ajax-handler-prop="{{ \Dash\Modules\MediaLibrary\Http\Ajax\FileManagerHandler::class }}"
        ></dash-media-library>
    </div>
@endsection

@push('before_body_end')
    <script src="{{ asset('dash/media-library/media-library.js') }}"></script>
@endpush
const mix = require('laravel-mix');

mix.setPublicPath('../public/');

mix.js('build/js/media-library.js', '')
    .sass('build/sass/media-library.scss', 'media-library.css');

if (mix.inProduction()) {
    mix.version();
} else {
    mix.sourceMaps();
}
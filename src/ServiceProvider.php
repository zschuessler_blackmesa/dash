<?php

namespace Dash;

use Dash\Modules\Theme\Providers\ThemeServiceProvider;
use Dash\Modules\Core\Providers\CoreServiceProvider;
use Dash\Module\LaravelModulesServiceProvider;
use Prologue\Alerts\AlertsServiceProvider;
use Spatie\Permission\PermissionServiceProvider;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Config
        // Register config file
        $this->publishes([
            __DIR__.'/Modules/Core/config/dash.php' => config_path('dash.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/Modules/Core/config/dash.php', 'dash'
        );

        // Register service providers
        $this->app->register(LaravelModulesServiceProvider::class);
        $this->app->register(CoreServiceProvider::class);
        $this->app->register(ThemeServiceProvider::class);
        $this->app->register(\Zschuessler\RouteToClass\ServiceProvider::class);
        $this->app->register(AlertsServiceProvider::class);
        $this->app->register(PermissionServiceProvider::class);
    }

    public function boot()
    {

    }
}

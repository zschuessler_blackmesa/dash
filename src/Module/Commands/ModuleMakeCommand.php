<?php

namespace Dash\Module\Commands;

use Dash\Module\Generators\ModuleGenerator;
use Illuminate\Console\Command;
use Illuminate\Support\Composer;

class ModuleMakeCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'module:make {name} {--overwrite}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new module.';

    public $composer;

    public function __construct(Composer $composer)
    {
        parent::__construct();

        $this->composer = $composer;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $moduleGenerator = (new ModuleGenerator([
                'name' => $this->argument('name')
            ]))
            ->overwrite($this->option('overwrite'))
            ->generate();

        $this->composer->dumpAutoloads();
    }
}

@extends('theme::layouts.default')

@section('content')
    <div class="row">
        <div class="col-xs-12 m-t-25 m-b-25">
            <h1 class="p-b-15">$MODULE_TITLE$</h1>

            <p>
                Edit this view in:
                <code>$MODULE_BASEPATH$/resources/index.blade.php</code>
            </p>
        </div>
    </div>
@endsection
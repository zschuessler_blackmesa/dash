<?php

namespace Dash\Module\Generators;

use Dash\Modules\Core\Traits\HasMagicMethods;
use Dash\Modules\Core\Traits\HasMagicProperties;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ModuleGenerator
{
    use HasMagicMethods {
        __call as callMagicMethod;
    }
    use HasMagicProperties;

    public $overwrite = false;

    public $basePath;

    public $templatesPath;

    public $name;

    public $module;

    public function __construct($options = null)
    {
        if (!data_get($options, 'name')) {
            throw new \Exception(
                'The `name` property must be set on instantiation: new Module(["name" => "ModuleName"])');
        }
        $this->name = data_get($options, 'name');
        $this->module = data_get($options, 'module') ?? collect(['name' => $this->name]);
        $this->basePath = data_get($options, 'basePath') ??  sprintf('%s/%s', config('dash.modules.path'), $this->name);
    }

    public function generate()
    {

        if (!$this->name) {
            throw new \Exception('A module name is required to generate a new module.');
        }

        if (!$this->templatesPath) {
            $this->templatesPath = __DIR__ . '/module-template';
        }

        $this->module = $this->getDefaultModuleOptions()
            ->merge(collect($this->module));

        if (!$this->basePath) {
            $this->basePath = sprintf('%s/%s', config('dash.modules.path'), $this->name);
        }

        // Assert module can be created
        $this->assertCanCreateModule();

        // Create all files from stubs
        $this->createModuleDirectories();
        $this->createModuleFiles();

        return $this;
    }

    public function getNamespaceFromConfig()
    {
        return config('dash.modules.namespace');
    }

    public function getDefaultModuleOptions()
    {
        // Module naming
        $name = ucwords($this->name);
        $slug = Str::slug(Str::kebab($name));
        $slugPlural = Str::slug(Str::plural(Str::kebab($name)));
        $title = collect(explode('-', $slug))
            ->transform(function($slugPart) {
                return ucwords($slugPart);
            })
            ->implode(' ');
        $titlePlural = Str::plural($title);

        // Namespaces & paths
        $namespace = $this->getNamespaceFromConfig() . '\\' . $name;
        $escapedAutoloadPath = str_replace('\\', '\\\\', $namespace) . '\\\\';

        // Service Provider
        $spNamespace = $this->getNamespaceFromPath(
            $namespace,
            "/Providers/{$name}ServiceProvider",
            $escape = true
        );

        // Dash
        $middleware = config('dash.default_middleware');
        $middlewareString = collect($middleware)
            ->transform(function($middlewareItem) {
                return "'{$middlewareItem}'";
            })
            ->implode(',');
        $middlewareString = sprintf('[%s]', $middlewareString);

        return collect([
            'name' => $name,
            'slug' => $slug,
            'slugPlural' => $slugPlural,
            'title' => $title,
            'titlePlural' => $titlePlural,

            'namespace' => $namespace,

            'escapedAutoloadPath' => $escapedAutoloadPath,

            'serviceProvider' => [
                'escapedNamespace' => $spNamespace
            ],

            'basePath' => $this->basePath,

            'dash' => [
                'middleware' => $middlewareString,
                'routeNamespace' => config('dash.default_route_namespace')
            ],

            'classes' => [
                'name' => function($fullFilePath) {
                    /** @var $stubReplacements Collection */
                    $stubReplacements = $this->getTemplateReplacementsCollection();

                    $className = $fullFilePath;

                    $stubReplacements->each(function($value, $key) use (&$className) {
                        if (!Str::contains($className, $key)) {
                            return;
                        }

                        if (is_callable($value)) {
                            // Dont allow recursive loops for file variables
                            return;
                        } else {
                            $className = str_replace($key, $value, $className);
                        }
                    });

                    $fileName = collect(explode('/', $className))->pop();
                    $className = collect(explode('.', $fileName))->shift();

                    return $className;
                },
                'namespace' => function($fullFilePath) {
                    $baseNamespace = $this->module->get('namespace');

                    $namespace = collect(explode('/', $fullFilePath));
                    $namespace->pop();
                    $namespace->transform(function($folderName) {
                        return ucwords($folderName);
                    });
                    $namespace = $baseNamespace . '\\' . $namespace->implode('\\');

                    // Remove \App from namespace since autoloader ignores it in composer.json
                    $finalNamespace = str_replace(
                        sprintf('%s\\App', $this->module->get('name')),
                        $this->module->get('name'),
                        $namespace
                    );

                    return $finalNamespace;
                }
            ]
        ]);
    }

    public function getNamespaceFromPath($namespace, $relativePath, $escape = false)
    {
        $baseNamespace = str_replace('\\', '\\\\', $namespace);
        $pathNamespace = str_replace('/', '\\\\', $relativePath);

        // Normalize slash count
        $namespace = str_replace('\\\\\\\\', '\\\\', $baseNamespace . '\\\\' . $pathNamespace);

        return $namespace;
    }

    public function assertCanCreateModule()
    {
        if (File::exists($this->basePath)
            && !$this->overwrite) {
            throw new \Exception('Module not created; folder exists. Use --overwrite to overwrite.');
        }

        return true;
    }

    public function createModuleFiles()
    {
        $directoriesIterator = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($this->templatesPath),
            \RecursiveIteratorIterator::SELF_FIRST,
            \RecursiveIteratorIterator::CATCH_GET_CHILD
        );
        $files = collect();
        foreach($directoriesIterator as $path => $file) {
            if (!$file->isDir()) {
                $files->push($path);
            }
        }

        // Convert to relative paths
        $files->transform(function($directoryPath) {
            $relativePath = Str::after($directoryPath, $this->templatesPath);

            return ltrim($relativePath, '/');
        });

        $files->each(function($filePath) {
            $this->createFileFromStub($filePath);
        });

        return $this;
    }

    public function getTemplateReplacementsCollection() : Collection
    {
        return collect($this->module)
            ->mapWithKeys(function ($value, $key) {
                // Allow one level of nesting of properties
                if (is_iterable($value)) {
                    $iterables= collect($value)
                        ->mapWithKeys(function ($childValue, $childKey) use ($value, $key) {
                            return [
                                sprintf('$MODULE_%s_%s$', strtoupper($key), strtoupper($childKey)) => $childValue
                            ];
                        });

                    return $iterables;
                }
                return [
                    sprintf('$MODULE_%s$', strtoupper($key)) => $value
                ];
            });
    }

    public function createFileFromStub($fileRelativePath)
    {
        // Path to stub templates: dash/dash/src/Module/Commands/module-template/
        $stubBasePath = $this->templatesPath .'/';

        /** @var $stubReplacements Collection */
        $stubReplacements = $this->getTemplateReplacementsCollection();

        /**
         * Replace stub placeholders
         */
        $stubFilePath = $stubBasePath . $fileRelativePath;
        $stubContents = file_get_contents($stubFilePath);
        $stubReplacements->each(function($value, $key) use (&$stubContents, $fileRelativePath) {
            if (!Str::contains($stubContents, $key)) {
                return;
            }

            if (is_callable($value)) {
                $value = call_user_func_array($value, [
                    'filePath' => $fileRelativePath
                ]);
            }

            $stubContents = str_replace($key, $value, $stubContents);
        });

        /**
         * Replace stub placeholders in file and folder names
         */
        $savePath = $fileRelativePath;
        $stubReplacements->each(function($value, $key) use (&$savePath) {
            if (!Str::contains($savePath, $key)) {
                return;
            }

            $savePath = str_replace($key, $value, $savePath);
        });

        File::put(
            $this->basePath . DIRECTORY_SEPARATOR . $savePath,
            $stubContents,
            644
        );

        return $this;
    }

    public function createDirectory($relativePath)
    {
        File::makeDirectory(
            $this->basePath . DIRECTORY_SEPARATOR . $relativePath,
            0755,
            true,
            true
        );

        return $this;
    }

    public function createModuleDirectories()
    {
        $directoriesIterator = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($this->templatesPath, \RecursiveDirectoryIterator::SKIP_DOTS),
            \RecursiveIteratorIterator::SELF_FIRST,
            \RecursiveIteratorIterator::CATCH_GET_CHILD
        );
        $directories = collect();
        foreach($directoriesIterator as $path => $file) {
            if ($file->isDir()) {
                $directories->push($path);
            }
        }

        // Convert to relative paths
        $directories->transform(function($directoryPath) {
            $relativePath = Str::after($directoryPath, $this->templatesPath);

            return ltrim($relativePath, '/');
        });

        // Create
        $directories->each(function($directoryPath) {
            $this->createDirectory($directoryPath);
        });

        return $this;
    }

    public function __call($calledKey, $arguments)
    {
        if (Str::startsWith($calledKey, ['module'])) {
            $moduleKey = lcfirst(str_replace('module', '', $calledKey));

            if ($this->module->has($moduleKey)) {
                dd($moduleKey);
            } else {
                dd('didnnt find');
            }
        }

        return $this->callMagicMethod($calledKey, $arguments);
    }
}

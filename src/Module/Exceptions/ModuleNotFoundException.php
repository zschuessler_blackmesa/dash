<?php

namespace Dash\Module\Exceptions;

class ModuleNotFoundException extends \Exception
{
}

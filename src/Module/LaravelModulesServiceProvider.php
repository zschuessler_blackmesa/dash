<?php

namespace Dash\Module;

use Dash\Module\Support\Stub;

class LaravelModulesServiceProvider extends ModulesServiceProvider
{
    /**
     * Booting the package.
     */
    public function boot()
    {
        $this->registerNamespaces();
        $this->registerModules();
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->registerServices();
        $this->registerProviders();
    }

    /**
     * {@inheritdoc}
     */
    protected function registerServices()
    {
        $this->app->singleton(Contracts\RepositoryInterface::class, function ($app) {
            $path = $app['config']->get('dash.modules.path');

            return new Laravel\LaravelFileRepository($app, $path);
        });
        $this->app->alias(Contracts\RepositoryInterface::class, 'modules');
    }
}

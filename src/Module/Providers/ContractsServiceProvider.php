<?php

namespace Dash\Module\Providers;

use Illuminate\Support\ServiceProvider;
use Dash\Module\Contracts\RepositoryInterface;
use Dash\Module\Laravel\LaravelFileRepository;

class ContractsServiceProvider extends ServiceProvider
{
    /**
     * Register some binding.
     */
    public function register()
    {
        $this->app->bind(RepositoryInterface::class, LaravelFileRepository::class);
    }
}
